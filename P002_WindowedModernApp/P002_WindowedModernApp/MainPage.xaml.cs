﻿namespace P002_WindowedModernApp
{
    using System;
    using System.Diagnostics;
    using System.Runtime.InteropServices;
    using Windows.UI.Xaml.Controls;


    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
        }

        private void DoStuff_Click(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            var foreground = User32.GetForegroundWindow();
            var handle = User32.FindWindowByCaption(IntPtr.Zero, "P002_WindowedModernApp");
            //var thing = Form1.SetParent()
            WINDOWINFO info = new WINDOWINFO
            {
                cbSize = (uint)Marshal.SizeOf<WINDOWINFO>()
            };

            User32.GetWindowInfo(handle, out info);

            User32.MoveWindow(handle, 100, 100, 200, 200, true);
            User32.SetWindowLong(handle, -16, WindowStyles.WS_TILEDWINDOW);
            var desktop = User32.GetDesktopWindow();
            if (!User32.ShowWindow(desktop, 5))
                Debugger.Break();
            
            if(!User32.ShowWindow(handle, 1))
                Debugger.Break();
        }
    }

    public static class WindowStyles
    {

        public static readonly Int32

      WS_BORDER = 0x00800000,

      WS_CAPTION = 0x00C00000,

      WS_CHILD = 0x40000000,

      WS_CHILDWINDOW = 0x40000000,

      WS_CLIPCHILDREN = 0x02000000,

      WS_CLIPSIBLINGS = 0x04000000,

      WS_DISABLED = 0x08000000,

      WS_DLGFRAME = 0x00400000,

      WS_GROUP = 0x00020000,

      WS_HSCROLL = 0x00100000,

      WS_ICONIC = 0x20000000,

      WS_MAXIMIZE = 0x01000000,

      WS_MAXIMIZEBOX = 0x00010000,

      WS_MINIMIZE = 0x20000000,

      WS_MINIMIZEBOX = 0x00020000,

      WS_OVERLAPPED = 0x00000000,

      WS_OVERLAPPEDWINDOW = WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU | WS_THICKFRAME | WS_MINIMIZEBOX | WS_MAXIMIZEBOX,

      WS_POPUP = unchecked((int)0x80000000),

      WS_POPUPWINDOW = WS_POPUP | WS_BORDER | WS_SYSMENU,

      WS_SIZEBOX = 0x00040000,

      WS_SYSMENU = 0x00080000,

      WS_TABSTOP = 0x00010000,

      WS_THICKFRAME = 0x00040000,

      WS_TILED = 0x00000000,

      WS_TILEDWINDOW = WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU | WS_THICKFRAME | WS_MINIMIZEBOX | WS_MAXIMIZEBOX,

      WS_VISIBLE = 0x10000000,

      WS_VSCROLL = 0x00200000;

    }

    public sealed class User32
    {
        internal User32()
        {
        }
        [DllImport("user32.dll")]
        internal static extern int GetAncestor(int hwnd, uint gaFlags);

        [DllImport("User32", CharSet = CharSet.Ansi, ExactSpelling = true)]
        public static extern IntPtr SetParent(IntPtr hWndChild, IntPtr hWndParent);

        [DllImport("user32.dll")]
        internal static extern bool MoveWindow(IntPtr hWnd, int X, int Y, int nWidth, int nHeight, bool bRepaint);

        [DllImport("user32.dll", CharSet = CharSet.Ansi, ExactSpelling = true)]
        public static extern int GetForegroundWindow();

        [DllImport("user32.dll", SetLastError = true)]
        public static extern IntPtr FindWindow(string lpClassName, string lpWindowName);

        [DllImport("user32.dll", CharSet = CharSet.Ansi, EntryPoint = "FindWindow", SetLastError = true)]
        public static extern IntPtr FindWindowByClass(string lpClassName, IntPtr zero);

        [DllImport("user32.dll", CharSet = CharSet.Ansi, EntryPoint = "FindWindow", SetLastError = true)]
        public static extern IntPtr FindWindowByCaption(IntPtr zero, string lpWindowName);

        [DllImport("user32.dll", SetLastError = true)]
        public static extern bool GetWindowInfo(IntPtr hWnd, out WINDOWINFO pwi);

        [DllImport("user32.dll", SetLastError = true)]
        public static extern IntPtr OpenInputDesktop(uint dwFlags, bool fInherit, uint dwDesiredAccess);

        [DllImport("user32.dll", SetLastError = true)]
        public static extern bool CloseDesktop(IntPtr hDesktop);

        [DllImport("kernel32.dll")]
        public static extern uint GetCurrentThreadId();

        [DllImport("user32.dll", SetLastError = true)]
        public static extern IntPtr GetThreadDesktop(uint dwThreadId);

        /// <summary>
        /// Changes an attribute of the specified window. The function also sets the 32-bit (long) value at the specified offset into the extra window memory.
        /// </summary>
        /// <param name="hWnd">A handle to the window and, indirectly, the class to which the window belongs..</param>
        /// <param name="nIndex">The zero-based offset to the value to be set. Valid values are in the range zero through the number of bytes of extra window memory, minus the size of an integer. To set any other value, specify one of the following values: GWL_EXSTYLE, GWL_HINSTANCE, GWL_ID, GWL_STYLE, GWL_USERDATA, GWL_WNDPROC </param>
        /// <param name="dwNewLong">The replacement value.</param>
        /// <returns>If the function succeeds, the return value is the previous value of the specified 32-bit integer.
        /// If the function fails, the return value is zero. To get extended error information, call GetLastError. </returns>
        [DllImport("user32.dll")]
        public static extern int SetWindowLong(IntPtr hWnd, int nIndex, int dwNewLong);

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);

        [DllImport("user32.dll", SetLastError = false)]
        public static extern IntPtr GetDesktopWindow();


    }

    public enum TaskbarPosition
    {
        Unknown = -1,
        Left,
        Top,
        Right,
        Bottom
    }

    public sealed class Shell32
    {
        internal Shell32()
        {
        }
        [DllImport("shell32.dll", SetLastError = true)]
        public static extern IntPtr SHAppBarMessage(ABM dwMessage, [In] ref APPBARDATA pData);
    }

    public struct RECT
    {
        public int left;
        public int top;
        public int right;
        public int bottom;
        public int Width { get { return right - left; } }
        public int Height { get { return bottom - top; } }
        public static RECT FromLTRB(int l, int t, int r, int b)
        {
            return new RECT
            {
                left = l,
                top = t,
                right = r,
                bottom = b
            };
        }
    }

    public struct WINDOWINFO
    {
        public uint cbSize;
        public RECT  rcWindow;
        public RECT  rcClient;
        public uint dwStyle;
        public uint dwExStyle;
        public uint dwWindowStatus;
        public uint  cxWindowBorders;
        public uint  cyWindowBorders;
        public ushort  atomWindowType;
        public ushort  wCreatorVersion;
    }

    public struct APPBARDATA
    {
        public uint cbSize;
        public IntPtr hWnd;
        public uint uCallbackMessage;
        public ABE uEdge;
        public RECT rc;
        public int lParam;
    }

    public sealed class ABS
    {
        public const int Autohide = 1;
        public const int AlwaysOnTop = 2;
        internal ABS()
        {
        }
    }

    public enum ABM : uint
    {
        New,
        Remove,
        QueryPos,
        SetPos,
        GetState,
        GetTaskbarPos,
        Activate,
        GetAutoHideBar,
        SetAutoHideBar,
        WindowPosChanged,
        SetState
    }

    public enum ABE : uint
    {
        Left,
        Top,
        Right,
        Bottom
    }
}
