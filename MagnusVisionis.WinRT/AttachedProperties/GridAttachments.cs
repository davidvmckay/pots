﻿namespace MagnusVisionis.WinRT.AttachedProperties
{
    using System;
    using System.Text.RegularExpressions;
    using Windows.UI.Xaml;
    using Windows.UI.Xaml.Controls;
    using WinRT_Grid = Windows.UI.Xaml.Controls.Grid;

    public static class Grid
    {
        #region borrowed and refactored: http://www.codeproject.com/Articles/427355/WPF-Stylizing-RowDefinitions-and-ColumnDefinitions#divTab2C#
        #region GridRowsDependencyProperty - allows styling grids with row defs
        static DependencyProperty RowsProperty = DependencyProperty.RegisterAttached(
            "Rows",
            typeof(string),
            typeof(Grid),
            new PropertyMetadata(string.Empty, new PropertyChangedCallback(RowsPropertyChanged)));

        public static string GetRows(WinRT_Grid @this)
        {
            return Convert.ToString(@this.GetValue(RowsProperty));
        }

        public static void SetRows(WinRT_Grid @this, string Value)
        {
            @this.SetValue(RowsProperty, Value);
        }

        private static void RowsPropertyChanged(object Sender, DependencyPropertyChangedEventArgs e)
        {
            WinRT_Grid @this = Sender as WinRT_Grid;
            if (@this == null)
                throw new Exception("Only elements of type 'Grid' can utilize the 'GridRows' attached property");
            DefineRows(@this);
        }

        private static void DefineRows(WinRT_Grid @this)
        {
            string[] Rows = GetRows(@this).Split(Convert.ToChar(","));
            @this.RowDefinitions.Clear();
            foreach (var Row in Rows)
            {
                switch (Row.Trim().ToLower())
                {
                    case "auto":
                        @this.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Auto) });
                        break;
                    case "*":
                        @this.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
                        break;
                    default:
                        if (Regex.IsMatch(Row, "^\\d+\\*$"))
                        {
                            @this.RowDefinitions.Add(new RowDefinition
                            {
                                Height = new GridLength(
                                    Convert.ToInt32(Row.Substring(0, Row.IndexOf(Convert.ToChar("*")))), GridUnitType.Star)
                            });
                        }
                        else if (Regex.IsMatch(Row, "^\\d+$"))
                        {
                            @this.RowDefinitions.Add(new RowDefinition { Height = new GridLength(Convert.ToDouble(Row), GridUnitType.Pixel) });
                        }
                        else
                        {
                            throw new Exception("The only acceptable value for the 'GridRows' " +
                              "attached property is a comma separated list comprised of the following options: Auto,*,x (where x is the pixel " +
                              "height of the row), x* (where x is the row height multiplier)");
                        }

                        break;
                }
            }
        }
        #endregion

        #region GridColumnsProperty - allows styling grids with col defs
        static DependencyProperty ColumnsProperty = DependencyProperty.RegisterAttached(
            "Columns",
            typeof(string),
            typeof(Grid),
            new PropertyMetadata(string.Empty, new PropertyChangedCallback(ColumnsPropertyChanged)));

        private static void ColumnsPropertyChanged(object Sender, DependencyPropertyChangedEventArgs e)
        {
            WinRT_Grid This = Sender as WinRT_Grid;
            if (This == null)
                throw new Exception("Only elements of type 'Grid' can " +
                  "utilize the 'GridColumns' attached property");
            DefineColumns(This);
        }

        public static string GetColumns(WinRT_Grid This)
        {
            return Convert.ToString(This.GetValue(ColumnsProperty));
        }

        public static void SetColumns(WinRT_Grid This, string Value)
        {
            This.SetValue(ColumnsProperty, Value);
        }

        private static void DefineColumns(WinRT_Grid This)
        {
            string[] Columns = GetColumns(This).Split(Convert.ToChar(","));
            This.ColumnDefinitions.Clear();
            foreach (var Column in Columns)
            {
                switch (Column.Trim().ToLower())
                {
                    case "auto":
                        This.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Auto) });
                        break;
                    case "*":
                        This.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
                        break;
                    default:
                        if (Regex.IsMatch(Column, "^\\d+\\*$"))
                        {
                            This.ColumnDefinitions.Add(new ColumnDefinition
                            {
                                Width =
                                    new GridLength(Convert.ToInt32(Column.Substring(0,
                                    Column.IndexOf(Convert.ToChar("*")))), GridUnitType.Star)
                            });
                        }
                        else if (Regex.IsMatch(Column, "^\\d+$"))
                        {
                            This.ColumnDefinitions.Add(new ColumnDefinition
                            {
                                Width = new GridLength(Convert.ToDouble(Column), GridUnitType.Pixel)
                            });
                        }
                        else
                        {
                            throw new Exception("The only acceptable value for the 'GridColumns' attached " +
                               "property is a comma separated list comprised of the following options: Auto,*,x (where x is the pixel width of the column), " +
                               "x* (where x is the column width multiplier)");
                        }
                        break;
                }
            }
        }
        #endregion
        #endregion
    }
}