﻿namespace MagnusVisionis.WinRT.Behaviors
{
    using Microsoft.Xaml.Interactivity;
    using Windows.UI.Xaml;
    using Windows.UI.Xaml.Controls;

    /// <summary>
    /// Allows a ViewModel to set the Focus of a control through a binding.
    /// </summary>
    /// <remarks>
    /// source: http://caliburnmicro.codeplex.com/discussions/222892
    /// </remarks>
    [TypeConstraint(typeof(Control))]
    public sealed class FocusBehavior : DependencyObject, IBehaviorAdapter
    {
        Control AssociatedObject = null;
        void OnAttached(DependencyObject o)
        {
            AssociatedObject = o as Control;
            AssociatedObject.GotFocus += (sender, args) => IsFocused = true;
            AssociatedObject.LostFocus += (sender, a) => IsFocused = false;
            AssociatedObject.Loaded += (s, a) => { if (HasInitialFocus || IsFocused) AssociatedObject.Focus(FocusState.Programmatic); };
        }

        static readonly DependencyProperty IsFocusedProperty =
            DependencyProperty.Register(
                "IsFocused",
                typeof(bool),
                typeof(FocusBehavior),
                new PropertyMetadata(false, (d, e) => { if ((bool)e.NewValue) ((FocusBehavior)d).AssociatedObject.Focus(FocusState.Programmatic); }));

        public bool IsFocused
        {
            get { return (bool)GetValue(IsFocusedProperty); }
            set { SetValue(IsFocusedProperty, value); }
        }

        static readonly DependencyProperty HasInitialFocusProperty =
            DependencyProperty.Register(
                "HasInitialFocus",
                typeof(bool),
                typeof(FocusBehavior),
                new PropertyMetadata(false, null));

        public bool HasInitialFocus
        {
            get { return (bool)GetValue(HasInitialFocusProperty); }
            set { SetValue(HasInitialFocusProperty, value); }
        }

        DependencyObject IBehaviorAdapter.AssociatedObject
        {
            get { throw new System.NotImplementedException(); }
        }

        public void Attach(DependencyObject associatedObject)
        {
            throw new System.NotImplementedException();
        }

        public void Detach()
        {
            throw new System.NotImplementedException();
        }
    }
}
