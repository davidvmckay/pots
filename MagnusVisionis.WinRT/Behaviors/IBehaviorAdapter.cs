﻿namespace MagnusVisionis.WinRT.Behaviors
{
    using Windows.UI.Xaml;

    // Summary:
    //     Interface implemented by all custom behaviors.
    public interface IBehaviorAdapter
    {
        // Summary:
        //     Gets the Windows.UI.Xaml.DependencyObject to which the Microsoft.Xaml.Interactivity.IBehavior
        //     is attached.
        DependencyObject AssociatedObject { get; }

        // Summary:
        //     Attaches to the specified object.
        //
        // Parameters:
        //   associatedObject:
        //     The Windows.UI.Xaml.DependencyObject to which the Microsoft.Xaml.Interactivity.IBehavior
        //     will be attached.
        void Attach(DependencyObject associatedObject);
        //
        // Summary:
        //     Detaches this instance from its associated object.
        void Detach();
    }
}