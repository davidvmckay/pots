﻿namespace MagnusVisionis.WinRT.Behaviors
{
    using System;
    using System.Collections.Generic;
    using Windows.System;
    using Microsoft.Xaml.Interactivity;
    using Windows.UI.Xaml;
    using Windows.UI.Xaml.Controls;

    /// <summary>
    /// Allows a ViewModel to set the Focus of a control through a binding.
    /// </summary>
    /// <remarks>
    /// source: http://caliburnmicro.codeplex.com/discussions/222892
    /// </remarks>
    [TypeConstraint(typeof(TextBox))]
    public sealed class TextChangedBehaviour : DependencyObject, IBehaviorAdapter
    {
        TextBox AssociatedObject = null;
        DispatcherTimer previousTextChangedTiming;
        void OnAttached(DependencyObject o)
        {
            AssociatedObject = o as TextBox;
            previousTextChangedTiming = new DispatcherTimer();
            previousTextChangedTiming.Interval = TimeSpan.FromMilliseconds(Delay);
            previousTextChangedTiming.Tick += (s, e) =>
            {
                OnTextChanged(AssociatedObject as TextBox);
                previousTextChangedTiming.Stop();
            };
            bool isShiftPressed = false;
            AssociatedObject.KeyUp += (s, e) =>
            {
                if (e.Key == VirtualKey.Shift || e.Key == VirtualKey.LeftShift || e.Key == VirtualKey.RightShift)
                {
                    isShiftPressed = false;
                }
            };
            AssociatedObject.KeyDown += (s, e) =>
            {
                if (e.Key == VirtualKey.Shift || e.Key == VirtualKey.LeftShift || e.Key == VirtualKey.RightShift)
                {
                    isShiftPressed = true;
                }

                //the space key is hereby white-listed
                if (VirtualKey.Space == e.Key)
                {
                    return;
                }

                int keyCode = (int)e.Key;
                if (keyCode < 48 || keyCode > 105 || new List<int>{91,92,93,95}.Contains(keyCode))
                {
                    e.Handled = RegexFilter != ".*";
                    return;
                }

                if (isShiftPressed && keyCode >= 48 && keyCode <= 57)
                {
                    e.Handled = RegexFilter != ".*";
                    return;
                }
            };
            AssociatedObject.TextChanged += (s,e) =>
            {
                if (previousTextChangedTiming.IsEnabled)
                {
                    previousTextChangedTiming.Stop();
                }

                previousTextChangedTiming.Start();
            };
            AssociatedObject.Text = LiveText ?? string.Empty;
        }

        static readonly DependencyProperty LiveTextProperty =
            DependencyProperty.Register(
                "LiveText",
                typeof(string),
                typeof(TextChangedBehaviour),
                new PropertyMetadata(string.Empty, (d, e) =>
                {
                    var behaviour = d as TextChangedBehaviour;
                    if (behaviour == null)
                    {
                        return;
                    }

                    //this can occur if the TextBox is hidden from view
                    if (behaviour.AssociatedObject == null)
                    {
                        return;
                    }

                    behaviour.AssociatedObject.Text = ((string)e.NewValue ?? string.Empty);
                }));

        static readonly DependencyProperty RegexFilterProperty =
            DependencyProperty.Register(
                "RegexFilter",
                typeof(string),
                typeof(TextChangedBehaviour),
                new PropertyMetadata(".*"));

        static readonly DependencyProperty DelayProperty =
            DependencyProperty.Register(
                "Delay",
                typeof(int),
                typeof(TextChangedBehaviour),
                new PropertyMetadata(500, (d, e) =>
                {
                    var behaviour = d as TextChangedBehaviour;
                    if (behaviour == null)
                    {
                        return;
                    }

                    //this can occur if the TextBox is hidden from view
                    if (behaviour.AssociatedObject == null)
                    {
                        return;
                    }

                    behaviour.previousTextChangedTiming.Interval = TimeSpan.FromMilliseconds((int)e.NewValue);
                }));

        void OnTextChanged(TextBox source)
        {
            if (source == null)
            {
                return;
            }

            LiveText = source.Text;
        }

        public string LiveText
        {
            get { return (string)GetValue(LiveTextProperty); }
            set { SetValue(LiveTextProperty, value); }
        }

        public string RegexFilter
        {
            get { return (string)GetValue(RegexFilterProperty); }
            set { SetValue(RegexFilterProperty, value); }
        }

        public int Delay
        {
            get { return (int)GetValue(DelayProperty); }
            set { SetValue(DelayProperty, value); }
        }

        DependencyObject IBehaviorAdapter.AssociatedObject
        {
            get { throw new NotImplementedException(); }
        }

        public void Attach(DependencyObject associatedObject)
        {
            throw new NotImplementedException();
        }

        public void Detach()
        {
            throw new NotImplementedException();
        }
    }
}