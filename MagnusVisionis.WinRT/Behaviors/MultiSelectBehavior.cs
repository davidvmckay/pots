﻿namespace MagnusVisionis.WinRT.Behaviors
{
    using System.Collections;
    using System.Collections.ObjectModel;
    using System.Collections.Specialized;
    using Microsoft.Xaml.Interactivity;
    using Windows.UI.Xaml;
    using Windows.UI.Xaml.Controls;
    using Windows.UI.Xaml.Interop;

    /// <summary>
    /// Adds Multiple Selection behavior to ListViewBase
    /// This adds capabilities to set/get Multiple selection from Binding (ViewModel)
    /// </summary>
    /// <remarks>
    /// pilfered from http://www.codeproject.com/Articles/412417/Managing-Multiple-selection-in-View-Model-NET-Metr
    /// ***** http://www.timmykokke.com/2013/09/behaviors-sdk/
    /// </remarks>
    [TypeConstraint(typeof(ListViewBase))]
    public sealed class MultiSelectBehavior : DependencyObject, IBehaviorAdapter
    {
        ListViewBase AssociatedObject = null;
        #region SelectedItems Attached Property
        static readonly DependencyProperty SelectedItemsProperty = DependencyProperty.Register(
            "SelectedItems",
            typeof(IEnumerable),
            typeof(MultiSelectBehavior),
            new PropertyMetadata(new ObservableCollection<object>(), PropertyChangedCallback));

        #endregion

        #region private
        private bool _selectionChangedInProgress; // Flag to avoid infinite loop if same viewmodel is shared by multiple controls
        #endregion

        public MultiSelectBehavior()
        {
            //SelectedItems = new ObservableCollection<object>();
        }

        public IEnumerable SelectedItems
        {
            get { return (IEnumerable)GetValue(SelectedItemsProperty); }
            set { SetValue(SelectedItemsProperty, value); }
        }

        void OnAttached(DependencyObject o)
        {
            AssociatedObject = o as ListViewBase;
            AssociatedObject.SelectionChanged += OnSelectionChanged;
        }

        void OnDetaching()
        {
            AssociatedObject.SelectionChanged -= OnSelectionChanged;

            // unhook the collection notifications if wired up too.
            var notifyingCollection = SelectedItems as INotifyCollectionChanged;
            if (notifyingCollection != null)
            {
                notifyingCollection.CollectionChanged -= SelectedItemsChanged;
            }

            // unhook the collection notifications if wired up too.
            var bindableVector = SelectedItems as IBindableObservableVector;
            if (bindableVector != null)
            {
                bindableVector.VectorChanged -= VectorChanged;
            }

        }

        static void PropertyChangedCallback(DependencyObject sender, DependencyPropertyChangedEventArgs args)
        {
            (sender as MultiSelectBehavior).WireNewSelectedItems(args.OldValue as IEnumerable, args.NewValue as IEnumerable);
        }

        void WireNewSelectedItems(IEnumerable originalValue, IEnumerable replacementValue)
        {
            var originalNotifyingCollection = originalValue as INotifyCollectionChanged;
            if (originalNotifyingCollection != null)
            {
                originalNotifyingCollection.CollectionChanged -= SelectedItemsChanged;
            }

            var originalBindableVector = originalValue as IBindableObservableVector;
            if (originalBindableVector != null)
            {
                originalBindableVector.VectorChanged -= VectorChanged;
            }

            if (AssociatedObject != null && replacementValue != null)
            {
                bool originalInProgressState = _selectionChangedInProgress;
                _selectionChangedInProgress = true;

                //replace selected items on the control with selected items in the new ObservableCollection
                ClearItems();

                foreach (var v in replacementValue)
                {
                    AddSelectedItem(v);
                }

                _selectionChangedInProgress = originalInProgressState;
            }

            var replacementNotifyingCollection = replacementValue as INotifyCollectionChanged;
            if (replacementNotifyingCollection != null)
            {
                replacementNotifyingCollection.CollectionChanged += SelectedItemsChanged;
            }

            var replacementBindableVector = replacementValue as IBindableObservableVector;
            if (replacementBindableVector != null)
            {
                replacementBindableVector.VectorChanged += VectorChanged;
            }

        }

        void AddSelectedItem(object v)
        {
            if (AssociatedObject.SelectionMode == ListViewSelectionMode.Extended || AssociatedObject.SelectionMode == ListViewSelectionMode.Multiple)
            {
                AssociatedObject.SelectedItems.Add(v);
            }
            else
            {
                AssociatedObject.SelectedItem = v;
            }
        }

        void SelectedItemsChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (_selectionChangedInProgress)
            {
                return;
            }

            var list = sender as IEnumerable;
            if (list != null)
            {
                bool originalInProgressState = _selectionChangedInProgress;
                _selectionChangedInProgress = true;

                ClearItems();            
                foreach (var o in list)
                {
                    AddSelectedItem(o);
                }

                _selectionChangedInProgress = originalInProgressState;
            }
        }

        void VectorChanged(IBindableObservableVector vector, object e)
        {
            SelectedItemsChanged(vector, null);
        }

        void ClearItems()
        {
            if (AssociatedObject.SelectionMode == ListViewSelectionMode.Extended || AssociatedObject.SelectionMode == ListViewSelectionMode.Multiple)
            {
                AssociatedObject.SelectedItems.Clear();
            }
        }

        void OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (_selectionChangedInProgress)
            {
                return;
            }

            var selectedItemsCollection = SelectedItems as IList;
            if (selectedItemsCollection == null)
            {
                return;
            }

            bool originalInProgressState = _selectionChangedInProgress;
            _selectionChangedInProgress = true;

            foreach (var item in e.RemovedItems)
            {
                if (selectedItemsCollection.Contains(item))
                {
                    selectedItemsCollection.Remove(item);
                }
            }

            foreach (var item in e.AddedItems)
            {
                if (!selectedItemsCollection.Contains(item))
                {
                    selectedItemsCollection.Add(item);
                }
            }

            _selectionChangedInProgress = originalInProgressState;
        }

        DependencyObject IBehaviorAdapter.AssociatedObject
        {
            get { throw new System.NotImplementedException(); }
        }

        public void Attach(DependencyObject associatedObject)
        {
            throw new System.NotImplementedException();
        }

        public void Detach()
        {
            throw new System.NotImplementedException();
        }
    }
}