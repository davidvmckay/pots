﻿namespace MagnusVisionis.WinRT.Converters
{
    using System;
    using System.Diagnostics;
    using Windows.Storage.Streams;
    using Windows.UI.Xaml.Data;
    using Windows.UI.Xaml.Media.Imaging;

    public sealed class BytesToBitmapImageConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            var bytes = value as byte[];

            if (object.ReferenceEquals(bytes, null))
            {
                return null;
            }

            if (bytes.Length == 0)
            {
                return null;
            }


            var bitmap = new BitmapImage();
            SetImageSourceAsync(bitmap, bytes);
            return bitmap;
        }

        async void SetImageSourceAsync(BitmapImage bitmap, byte[] bytes)
        {
            try
            {
                using (var stream = new InMemoryRandomAccessStream())
                using (var writer = new DataWriter(stream))
                {
                    writer.WriteBytes(bytes);
                    await writer.StoreAsync();
                    stream.Seek(0);
                    await bitmap.SetSourceAsync(stream);
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            var bitmap = value as BitmapImage;

            if (object.ReferenceEquals(bitmap, null))
            {
                return null;
            }

            if (targetType != typeof(byte[]))
            {
                return null;
            }

            return null;
        }
    }
}