﻿namespace MagnusVisionis.WinRT.Converters
{
    using System;
    using Windows.UI.Xaml.Data;

    /// <summary>
    /// Value converter that translates true to false and vice versa.
    /// </summary>
    public sealed class TrueIfNotEmptyConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            return !string.IsNullOrWhiteSpace(value as string);
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotSupportedException("BooleanTestForEmptyConverter is one-way only.");
        }
    }
}