﻿namespace MagnusVisionis.WinRT.Converters
{
    using System;
    using Windows.UI.Xaml.Data;

    public sealed class StringFormatConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            if (parameter == null)// No format provided.
                return value;

            return string.Format(parameter as string, value);
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            return value;
        }
    }
}
