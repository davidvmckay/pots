﻿

#pragma checksum "C:\Code\Pots\P003_LiveVideoProcessor\P003_LiveVideoProcessor\Media capture using capture device sample\C# and C++\BasicCapture.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "5D05B4D426141749C5D300E883EF01C1"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MediaCapture
{
    partial class BasicCapture : global::SDKTemplate.Common.LayoutAwarePage
    {
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Windows.UI.Xaml.Build.Tasks"," 4.0.0.0")]
        private global::Windows.UI.Xaml.Controls.Grid LayoutRoot; 
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Windows.UI.Xaml.Build.Tasks"," 4.0.0.0")]
        private global::Windows.UI.Xaml.Controls.Grid Input; 
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Windows.UI.Xaml.Build.Tasks"," 4.0.0.0")]
        private global::Windows.UI.Xaml.Controls.Grid Output; 
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Windows.UI.Xaml.Build.Tasks"," 4.0.0.0")]
        private global::Windows.UI.Xaml.Controls.Canvas imageCanvas1; 
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Windows.UI.Xaml.Build.Tasks"," 4.0.0.0")]
        private global::Windows.UI.Xaml.Controls.Image imageElement1; 
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Windows.UI.Xaml.Build.Tasks"," 4.0.0.0")]
        private global::Windows.UI.Xaml.Controls.Canvas playbackCanvas1; 
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Windows.UI.Xaml.Build.Tasks"," 4.0.0.0")]
        private global::Windows.UI.Xaml.Controls.MediaElement playbackElement1; 
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Windows.UI.Xaml.Build.Tasks"," 4.0.0.0")]
        private global::Windows.UI.Xaml.Controls.Canvas previewCanvas1; 
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Windows.UI.Xaml.Build.Tasks"," 4.0.0.0")]
        private global::Windows.UI.Xaml.Controls.CaptureElement previewElement1; 
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Windows.UI.Xaml.Build.Tasks"," 4.0.0.0")]
        private global::Windows.UI.Xaml.Controls.Slider sldBrightness; 
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Windows.UI.Xaml.Build.Tasks"," 4.0.0.0")]
        private global::Windows.UI.Xaml.Controls.Slider sldContrast; 
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Windows.UI.Xaml.Build.Tasks"," 4.0.0.0")]
        private global::Windows.UI.Xaml.Controls.Button btnStartDevice1; 
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Windows.UI.Xaml.Build.Tasks"," 4.0.0.0")]
        private global::Windows.UI.Xaml.Controls.Button btnStartPreview1; 
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Windows.UI.Xaml.Build.Tasks"," 4.0.0.0")]
        private global::Windows.UI.Xaml.Controls.Button btnStartStopRecord1; 
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Windows.UI.Xaml.Build.Tasks"," 4.0.0.0")]
        private global::Windows.UI.Xaml.Controls.Button btnTakePhoto1; 
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Windows.UI.Xaml.Build.Tasks"," 4.0.0.0")]
        private global::Windows.UI.Xaml.Controls.TextBlock InputTextBlock1; 
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Windows.UI.Xaml.Build.Tasks"," 4.0.0.0")]
        private global::Windows.UI.Xaml.VisualState DefaultLayout; 
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Windows.UI.Xaml.Build.Tasks"," 4.0.0.0")]
        private global::Windows.UI.Xaml.VisualState Below768Layout; 
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Windows.UI.Xaml.Build.Tasks"," 4.0.0.0")]
        private bool _contentLoaded;

        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Windows.UI.Xaml.Build.Tasks"," 4.0.0.0")]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public void InitializeComponent()
        {
            if (_contentLoaded)
                return;

            _contentLoaded = true;
            global::Windows.UI.Xaml.Application.LoadComponent(this, new global::System.Uri("ms-appx:///BasicCapture.xaml"), global::Windows.UI.Xaml.Controls.Primitives.ComponentResourceLocation.Application);
 
            LayoutRoot = (global::Windows.UI.Xaml.Controls.Grid)this.FindName("LayoutRoot");
            Input = (global::Windows.UI.Xaml.Controls.Grid)this.FindName("Input");
            Output = (global::Windows.UI.Xaml.Controls.Grid)this.FindName("Output");
            imageCanvas1 = (global::Windows.UI.Xaml.Controls.Canvas)this.FindName("imageCanvas1");
            imageElement1 = (global::Windows.UI.Xaml.Controls.Image)this.FindName("imageElement1");
            playbackCanvas1 = (global::Windows.UI.Xaml.Controls.Canvas)this.FindName("playbackCanvas1");
            playbackElement1 = (global::Windows.UI.Xaml.Controls.MediaElement)this.FindName("playbackElement1");
            previewCanvas1 = (global::Windows.UI.Xaml.Controls.Canvas)this.FindName("previewCanvas1");
            previewElement1 = (global::Windows.UI.Xaml.Controls.CaptureElement)this.FindName("previewElement1");
            sldBrightness = (global::Windows.UI.Xaml.Controls.Slider)this.FindName("sldBrightness");
            sldContrast = (global::Windows.UI.Xaml.Controls.Slider)this.FindName("sldContrast");
            btnStartDevice1 = (global::Windows.UI.Xaml.Controls.Button)this.FindName("btnStartDevice1");
            btnStartPreview1 = (global::Windows.UI.Xaml.Controls.Button)this.FindName("btnStartPreview1");
            btnStartStopRecord1 = (global::Windows.UI.Xaml.Controls.Button)this.FindName("btnStartStopRecord1");
            btnTakePhoto1 = (global::Windows.UI.Xaml.Controls.Button)this.FindName("btnTakePhoto1");
            InputTextBlock1 = (global::Windows.UI.Xaml.Controls.TextBlock)this.FindName("InputTextBlock1");
            DefaultLayout = (global::Windows.UI.Xaml.VisualState)this.FindName("DefaultLayout");
            Below768Layout = (global::Windows.UI.Xaml.VisualState)this.FindName("Below768Layout");
        }
    }
}



