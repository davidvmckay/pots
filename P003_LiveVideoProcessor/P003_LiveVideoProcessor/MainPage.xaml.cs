﻿namespace P003_LiveVideoProcessor
{
    using System;
    using System.Diagnostics;
    using System.IO;
    using System.Text;
    using System.Threading.Tasks;
    using Windows.Media.Capture;
    using Windows.Media.MediaProperties;
    using Windows.Storage.Streams;
    using Windows.UI.Xaml.Controls;

    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
        }

        protected override void OnNavigatedTo(Windows.UI.Xaml.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            Initialize();
        }

        async void Initialize()
        {
            var mediaCapture = new MediaCapture();
            await mediaCapture.InitializeAsync();
            MainCaptureElement.Source = mediaCapture;
            await mediaCapture.StartPreviewAsync();
            var profile = MediaEncodingProfile.CreateAvi(VideoEncodingQuality.Auto);

            byte[] data;
            using(var memory = new MemoryStream())
            using (var stream = memory.AsRandomAccessStream())
            {
                await mediaCapture.StartRecordToStreamAsync(profile, stream);
                await Task.Delay(1000);
                await mediaCapture.StopRecordAsync();

                memory.Seek(0, SeekOrigin.Begin);
                var riff = new RiffParserDemo2.RiffParser();
                riff.OpenStream(memory);
                var d = new RiffParserDemo2.DecodeHeader(riff);
                d.ProcessMainAVI();
            }

        }
    }
}