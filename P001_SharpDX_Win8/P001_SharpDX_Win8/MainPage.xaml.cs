﻿namespace P001_SharpDX_Win8
{
    using System;
    using CommonDX;
    using Windows.Graphics.Display;
    using Windows.UI.Xaml;
    using Windows.UI.Xaml.Controls;

    public sealed partial class MainPage : Page, IDisposable
    {
        DeviceManager deviceManager;
        SwapChainPanelTarget target;
        
        public static readonly DependencyProperty DesignModeD3DRenderingProperty =
            DependencyProperty.Register("DesignModeD3DRendering", typeof(Boolean), typeof(MainPage), new PropertyMetadata(false));

        public Boolean DesignModeD3DRendering
        {
            get
            {
                return (Boolean)GetValue(DesignModeD3DRenderingProperty);
            }
            set
            {
                SetValue(DesignModeD3DRenderingProperty, value);
            }
        }

        public MainPage()
        {
            this.InitializeComponent();

            // Do D3D initialization when element is loaded, because DesignModeD3DRendering is yet not set in ctor
            this.Loaded += Direct3DUserControl_Loaded;            
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        void Direct3DUserControl_Loaded(object sender, RoutedEventArgs e)
        {
            // Do not initialize D3D in design mode as default, since it may cause designer crashes
            if (Windows.ApplicationModel.DesignMode.DesignModeEnabled && !DesignModeD3DRendering)
                return;

            // Safely dispose any previous instance
            // Creates a new DeviceManager (Direct3D, Direct2D, DirectWrite, WIC)
            deviceManager = new DeviceManager();

            // Use current control as the rendering target (Initialize SwapChain, RenderTargetView, DepthStencilView, BitmapTarget)
            target = new SwapChainPanelTarget(ContentPanel);

            // Add Initializer to device manager
            deviceManager.OnInitialize += target.Initialize;

            // New CubeRenderer
            var cubeRenderer = new CubeRenderer();
            cubeRenderer.ShowCube = true;
            deviceManager.OnInitialize += cubeRenderer.Initialize;
            target.OnRender += cubeRenderer.Render;

            // Initialize the device manager and all registered deviceManager.OnInitialize             
            try
            {
                deviceManager.Initialize(DisplayInformation.GetForCurrentView().LogicalDpi);
                DisplayInformation.GetForCurrentView().DpiChanged += DisplayInformation_LogicalDpiChanged;
            }
            catch (Exception ex)
            {
                //DisplayInformation.GetForCurrentView() will throw exception in designer
                deviceManager.Initialize(96.0f);
            }

            // Setup rendering callback
            CompositionTargetEx.Rendering += CompositionTarget_Rendering;

            this.LayoutUpdated += Direct3DUserControl_LayoutUpdated;
        }

        void Direct3DUserControl_LayoutUpdated(object sender, object e)
        {
            //TODO: handle updated Layout
        }

        void DisplayInformation_LogicalDpiChanged(DisplayInformation displayInformation, object sender)
        {
            deviceManager.Dpi = displayInformation.LogicalDpi;
            //TODO: handle other value affected by DPI change
        }

        void CompositionTarget_Rendering(object sender, object e)
        {
            target.RenderAll();
            target.Present();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.BottomAppBar.IsOpen = true;
        }
    }
}