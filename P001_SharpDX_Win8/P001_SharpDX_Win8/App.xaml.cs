﻿namespace P001_SharpDX_Win8
{
    using System;
    using System.Diagnostics;
    using Windows.ApplicationModel;
    using Windows.ApplicationModel.Activation;
    using Windows.UI.Xaml;

    sealed partial class App : Application, IDisposable
    {
        public App()
        {
            this.InitializeComponent();
            this.Suspending += OnSuspending;
        }

        protected override void OnLaunched(LaunchActivatedEventArgs e)
        {
#if DEBUG
            if (Debugger.IsAttached)
            {
                this.DebugSettings.EnableFrameRateCounter = true;
            }
#endif

            Dispose();
            Window.Current.Content = new MainPage();
            Window.Current.Activate();
        }

        private void OnSuspending(object sender, SuspendingEventArgs e)
        {
            var deferral = e.SuspendingOperation.GetDeferral();
            //TODO: Save application state and stop any background activity
            deferral.Complete();
        }

        public void Dispose()
        {
            var root = Window.Current.Content as MainPage;
            if (root != null)
                root.Dispose();

            Window.Current.Content = null;
        }
    }
}