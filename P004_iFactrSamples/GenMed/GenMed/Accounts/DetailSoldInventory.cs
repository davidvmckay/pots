using System;
using System.Collections.Generic;
using iFactr.Core.Layers;

namespace GenMed.Accounts
{
    public class DetailSoldInventory : iLayer 
    {
        public override void Load(Dictionary<string, string> parameters)
        {
            string accountId = parameters["Account"];
            Data.Account account = Data.Provider.GetAccountById(accountId);

            Title = "Sold Inventory";

            iList list = new iList();
            foreach (Data.InventoryItem item in account.SoldInventory)
            {
                list.Items.Add(new iItem(null, string.Format("{0} {1} (Qty-{2})", item.Product.Model, item.Serial, item.Quantity), 
				                             string.Format ("Sold: {0} DTE: {1} {2}", item.DateSold.ToString("M/d/yyyy"), item.DaysToExpire, item.Product.Description)));
            }
            if (list.Items.Count == 0)
            {
                list.Items.Add(new iItem(null, "No Sold Inventory", "For this Account"));
            }
            Items.Add(list);
        }
	}
}