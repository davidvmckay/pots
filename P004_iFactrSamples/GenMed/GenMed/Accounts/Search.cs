using GenMed.Data;
using iFactr.Core.Controls;
using iFactr.Core.Layers;
using iFactr.Core.Styles;
using System;
using System.Collections.Generic;

namespace GenMed.Accounts
{
    public class Search : iLayer, IMasterLayer
    {
        public override void Load(Dictionary<string, string> parameters)
        {
            Title = "Accounts";

            // when setting styles, remember that the color will be applied to every platform and may not look desirable on certain targets
            LayerStyle.SectionHeaderColor = new Style.Color("#dddddd");

            SearchList list = new SearchList();

            string navUri;
            if (parameters.ContainsKey("Type") && parameters.ContainsKey("Model"))
            {
                Name = Name + "ProdOrder";
                navUri = string.Format("Products/{0}/{1}/Sell/{2}/", parameters["Type"], parameters["Model"], parameters["Source"]);
            }
            else
            {
                navUri = "Accounts/";
            }

            foreach (Account account in Data.Provider.Accounts)
            {
                list.Items.Add(new iItem(navUri + account.Id, account.Name, string.Format("{0} - {1}, {2}", account.Id, account.Address.City, account.Address.State))
                {
                    Icon = new Icon("Headshots/" + account.Id + ".jpg"),
                });
            }
            Items.Add(list);

            // the About button is only wanted if this layer is on the first tab.
            // in any other case, the layer will be further down the history stack and the button would be inappropriate
            if (NavContext.NavigatedActiveTab == 0)
                ActionButtons.Add(new Button("iFactr", "About") { Image = new Icon("as-about.png"), } );
        }
    }
}