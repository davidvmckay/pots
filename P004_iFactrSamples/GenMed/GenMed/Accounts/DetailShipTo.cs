﻿using System.Collections.Generic;
using iFactr.Core.Layers;

namespace GenMed.Accounts
{
    public class DetailShipTo : iLayer 
    {
        public override void Load(Dictionary<string, string> parameters)
        {
            string accountId = parameters["Account"];
            Data.Account account = Data.Provider.GetAccountById(accountId);

            Title = "Ship To";

            iList list = new iList();
            foreach (Data.Address address in account.MailToAddresses)
            {
                list.Items.Add(new iItem(null, address.Street1, address.City + ", " + address.State + "  " + address.Zip, true));
            }
            Items.Add(list);
        }
    }
}