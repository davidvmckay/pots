using System;
using System.Collections.Generic;
using System.Linq;
using iFactr.Core.Controls;
using iFactr.Core.Layers;

namespace GenMed.Accounts
{
    public class Detail : iLayer
    {
        public override void Load(Dictionary<string, string> parameters)
		{
			string accountId = parameters ["Account"];
			string orderId = parameters.ContainsKey ("Order") ? parameters ["Order"] : null;
			Data.Account account = Data.Provider.GetAccountById (accountId);

			// if the orderId is not null, we are submitting an order
			if (orderId != null) 
			{
				Data.Order order = Data.Provider.GetOrderById (orderId);
				Data.Provider.SubmitOrder (order);
			}

			Title = account.Name;

			// create the panel
			iPanel panel = new iPanel ();
			panel.Header = "Account Detail";
			
			// insert an image on the righthand side
			panel.InsertImageFloatRight ("Headshots/" + account.Id + ".jpg", "100", "100");
			
			// append an image
			panel.AppendSubHeading (account.Name);
			panel.AppendItalicLine ("Account Num: " + account.Id);
			panel.AppendLine ();
			panel.AppendLine ();
			
			// diplay account detail (in text)
			panel.AppendLine (account.Address.Street1);
			panel.AppendLine (account.Address.City + ", " + account.Address.State + "  " + account.Address.Zip);
			panel.AppendLine ();
			if (!string.IsNullOrEmpty (account.Email)) 
            {
				panel.AppendEmail (account.Email);
				panel.AppendLine ();
			}
			if (!string.IsNullOrEmpty (account.Phone)) {
				panel.AppendTelephone (account.Phone);
			}

            iMenu menu = new iMenu();
            menu.Items.Add(new iItem(string.Format("Accounts/{0}/ShipTo", accountId), "Ship To Addresses") { Icon = new Icon("ShipTo.png") });
            menu.Items.Add(new iItem(string.Format("Accounts/{0}/Inventory", accountId), "Inventory") { Icon = new Icon("Inventory.png") });
            menu.Items.Add(new iItem(string.Format("Accounts/{0}/SoldInventory", accountId), "Sold Inventory") { Icon = new Icon("SoldInventory.png") });

            Items.Add(panel);
            Items.Add(menu);

            ActionButtons.Add(new Button("Warehouse Sale", HtmlTextExtensions.VirtualPath(string.Format("Transactions/{0}/Sell/Warehouse/New", accountId))) { Image = new Icon() { Location = "as-sellwarehouse.png" } });
            ActionButtons.Add(new Button("Trunk Sale", HtmlTextExtensions.VirtualPath(string.Format("Transactions/{0}/Sell/Warehouse/New", accountId))) { Image = new Icon() { Location = "as-selltrunk.png" } });
        }
    }
}