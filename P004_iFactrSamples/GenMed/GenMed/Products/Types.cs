using System.Collections.Generic;
using iFactr.Core.Controls;
using iFactr.Core.Layers;

namespace GenMed.Products
{
    public class Types : iLayer, IMasterLayer
    {
        public override void Load(Dictionary<string, string> parameters)
        {
            string navUri;

            if (parameters.ContainsKey("Account") && parameters.ContainsKey("Order"))
            {
                navUri = string.Format("Transactions/{0}/Sell/Warehouse/{1}/Items/", parameters["Account"], parameters["Order"]);
            }
            else
            {
                navUri = "Products/";
            }

            // layer equality is determined by Name.
            // it can be used to have two different layers take the same place in the history stack or have two instances of the same layer take different positions in the history stack
            Name = navUri.Replace("/", ".");
            Title = "Products";

            var menu = new iMenu();
            menu.Header = string.Empty;

            foreach (Data.ProductType type in Data.Provider.ProductTypes)
            {
                menu.Items.Add(new iItem(navUri + type.TypeCode, type.Description) { Icon = new iFactr.Core.Controls.Icon("Products/" + type.TypeCode + ".jpg") });
            }

            Items.Add(menu);

            // the About button is only wanted if this layer is on the third tab.
            // in any other case, the layer will be further down the history stack and the button would be inappropriate
            if (NavContext.NavigatedActiveTab == 2)
                ActionButtons.Add(new Button("iFactr", "About") { Image = new Icon("as-about.png"), });
        }
    }
}