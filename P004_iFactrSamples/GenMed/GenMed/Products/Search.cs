using System;
using System.Collections.Generic;
using iFactr.Core.Layers;
using iFactr.Core.Styles;

namespace GenMed.Products
{
    public class Search : iLayer, IMasterLayer
    {
        public override void Load(Dictionary<string, string> parameters)
        {
            // when setting styles, remember that the color will be applied to every platform and may not look desirable on certain targets
            LayerStyle.SectionHeaderColor = new Style.Color("#dddddd");

            string type = "0";
            parameters.TryGetValue("Type", out type);

            string navUri = string.Empty;
            if (parameters.ContainsKey("Account") && parameters.ContainsKey("Order"))
            {
                navUri = string.Format("Transactions/{0}/Sell/Warehouse/{1}/Items/{2}/", parameters["Account"], parameters["Order"], type);
            }
            else
            {
                navUri = string.Format("Products/{0}/", type);
            }

            // layer equality is determined by Name.
            // it can be used to have two different layers take the same place in the history stack or have two instances of the same layer take different positions in the history stack
            Name = navUri.Replace("/", ".");
            Title = Data.Provider.GetDescriptionForProductType(type);

            SearchList list = new SearchList();

            foreach (Data.Product product in Data.Provider.GetProductsForType(type))
            {
                list.Items.Add(new iItem(navUri + product.Model, product.Model, product.Description) { Icon = new iFactr.Core.Controls.Icon("Products/" + product.Model.Replace(" ", "") + ".jpg") });
            }

            Items.Add(list);
        }
    }
}