using System.Collections.Generic;
using iFactr.Core.Layers;
using iFactr.Core.Controls;

namespace GenMed
{
	public class About: iLayer
	{
		public override void Load (Dictionary<string, string> parameters)
		{
			Title = "About iFactr";

			iPanel panel = new iPanel();
			panel.AppendLine();
			panel.InsertImage("img-ifactr.png");
			panel.AppendLine();
			
			iMenu list = new iMenu();
			list.Items.Add(new iItem("mailto:sales@ifactr.com?subject=I want to schedule an iFactr Demo&body=Please contact me at the following phone or email address to schedule an iFactr demo.", "Schedule Demo") { Icon = new Icon("icn-demo.png") });
            list.Items.Add(new iItem("http://youtube.com/iFactr", "YouTube Demo's & Tutorials") { Icon = new Icon("icn-youtube.png") });
            list.Items.Add(new iItem("tel:1-800-265-1620", "Call Us") { Icon = new Icon("icn-contact.png") });
            list.Items.Add(new iItem("http://ifactr.com", "iFactr Website") { Icon = new Icon("icn-website.png") });

			Items.Add(panel);
			Items.Add(list);
		}
	}
}