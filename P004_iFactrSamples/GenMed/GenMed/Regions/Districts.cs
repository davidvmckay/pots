using System;
using System.Collections.Generic;
using System.Linq;
using iFactr.Core.Layers;

namespace GenMed.Regions
{
    public class Districts : iLayer, IMasterLayer
    {
        public override void Load(Dictionary<string, string> parameters)
        {
            string regionId = parameters["Region"];
            Reporting.POPending.Data.Region currentRegion = Reporting.POPending.Data.Provider.GeographyDataSet.Regions.FirstOrDefault(region => region.Id == regionId);

            Title = currentRegion.Name;

            iList list = new iList();
            list.Header = "Districts";

            list.Items.Add(new iItem(string.Format("Regions/{0}/All", currentRegion.Id), "All Accounts"));
            list.Items.Add(new iItem(string.Format("Regions/{0}/All", currentRegion.Id), "All Hospitals"));
            list.Items.Add(new iItem(string.Format("Regions/{0}/All", currentRegion.Id), "All Clinics"));

            foreach (Reporting.POPending.Data.District district in currentRegion.Districts)
            {
                list.Items.Add(new iItem(string.Format("Regions/{0}/{1}", currentRegion.Id, district.Id), district.Name, true));
            }

            Items.Add(list);
        }
    }
}