using System.Collections.Generic;
using iFactr.Core.Controls;
using iFactr.Core.Layers;

namespace GenMed.Regions
{
    public class Regions : iLayer, IMasterLayer
    {
        public override void Load(Dictionary<string, string> parameters)
        {
            Title = "Regions";

            var menu = new iMenu();
            menu.Header = string.Empty;

            foreach (Reporting.POPending.Data.Region region in Reporting.POPending.Data.Provider.GeographyDataSet.Regions)
            {
                menu.Items.Add(new iItem("Regions/" + region.Id, region.Name, true)
                {
                    Icon = new iFactr.Core.Controls.Icon("Regions/" + region.Id + ".png")
                });
            }

            Items.Add(menu);

            ActionButtons.Add(new Button("iFactr", "About") { Image = new Icon("as-about.png"), });
        }
    }
}