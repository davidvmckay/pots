﻿using System.Collections.Generic;
using System.Linq;

namespace GenMed.Data
{
    public class Geography
    {
        public string Name { get; set; }
        public List<Region> Regions { get; set; }

        public IEnumerable<Account> GetAccounts()
        {
            return Regions.SelectMany(region => region.GetAccounts());
        }
        
        public Geography()
        {
            Regions = new List<Region>();
        }
    }
}