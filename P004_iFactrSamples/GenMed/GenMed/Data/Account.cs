using System.Collections.Generic;

namespace GenMed.Data
{
    public class Account
    {
        public string Name { get; set; }
        public string Id { get; set; }
        public string Type { get; set; }
        public Address Address { get; set; }
        public Address MailToAddress { get; set; }
        public List<Address> MailToAddresses { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public List<InventoryItem> Inventory { get; set; }
        public List<InventoryItem> SoldInventory { get; set; }

        public Account()
        {
            MailToAddresses = new List<Address>();
            Inventory = new List<InventoryItem>();
            SoldInventory = new List<InventoryItem>();
        }
    }
}