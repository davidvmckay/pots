﻿using System.Collections.Generic;
using System.Linq;

namespace GenMed.Data
{
    public class District
    {
        public string Name { get; set; }
        public string Id { get; set; }
        public List<Territory> Territories { get; set; }

        public IEnumerable<Account> GetAccounts()
        {
            return Territories.SelectMany(territory => territory.Accounts);
        }

        public District()
        {
            Territories = new List<Territory>();
        }
    }
}