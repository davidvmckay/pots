using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Serialization;

using iFactr.Core;

namespace GenMed.Data
{
    public static class Provider
    {
        public static List<Account> Accounts { get; set; }
        public static List<Account> AccountDetails = new List<Account>();
        public static List<Order> PendingOrders = new List<Order>();
        public static List<ProductType> ProductTypes { get; set; }

        static Provider()
        {
            // we want to load the data from our xml files as soon as they are needed
            string fileAccounts = Path.Combine(iApp.Factory.ApplicationPath, "Accounts.xml");
            string fileProducts = Path.Combine(iApp.Factory.ApplicationPath, "Products.xml");

            XmlSerializer xmlAccounts = new XmlSerializer(typeof(List<Account>),
                new Type[] { 
                    typeof(Account), 
                    typeof(List<Address>), 
                    typeof(Address), 
                    typeof(List<InventoryItem>), 
                    typeof(InventoryItem), 
                    typeof(Product), 
                });

            XmlSerializer xmlProducts = new XmlSerializer(typeof(List<ProductType>),
            new Type[] { 
                    typeof(ProductType), 
                    typeof(List<Product>), 
                    typeof(Product),
                });

            // iApp.File.Read will give us a byte array that can be passed to a stream for deserialization
            Accounts = (List<Account>)xmlAccounts.Deserialize(new MemoryStream(iApp.File.Read(fileAccounts)));
            ProductTypes = (List<ProductType>)xmlProducts.Deserialize(new MemoryStream(iApp.File.Read(fileProducts)));
        }

        public static Account GetAccountById(string id)
        {
            Account retval = AccountDetails.FirstOrDefault(account => account.Id == id);

            if (retval == null)
            {
                // lazy-load account info
                string fileAccountDetail = Path.Combine(iApp.Factory.ApplicationPath, Path.Combine("Accounts", string.Format("{0}.xml", id)));

                XmlSerializer xmlAccountDetail = new XmlSerializer(typeof(Account),
                new Type[] {
                    typeof(List<Address>), 
                    typeof(Address), 
                    typeof(List<InventoryItem>), 
                    typeof(InventoryItem), 
                    typeof(Product), 
                });

                retval = (Account)xmlAccountDetail.Deserialize(new MemoryStream(iApp.File.Read(fileAccountDetail)));
            }

            return retval;
        }

        public static string GetDescriptionForProductType(string code)
        {
            ProductType prodType = ProductTypes.FirstOrDefault(type => type.TypeCode == code);
            return prodType == null ? string.Empty : prodType.Description;
        }

        public static IEnumerable<Product> GetProductsForType(string code)
        {
            ProductType prodType = ProductTypes.FirstOrDefault(type => type.TypeCode == code);
            return prodType == null ? new List<Product>() : prodType.Products;
        }

        public static Product GetProductById(string type, string model)
        {
            return string.IsNullOrEmpty(type) ? GetProductById(model) : GetProductsForType(type).FirstOrDefault(product => product.Model == model);
        }

        public static Product GetProductById(string model)
        {
            foreach (var type in ProductTypes)
            {
                var item = type.Products.FirstOrDefault(product => product.Model == model);
                if (item != null) return item;
            }
            return null;
        }

        public static Order GetOrderById(string id)
        {
            return PendingOrders.FirstOrDefault(order => order.Id == id);
        }

        public static void SaveOrder(Order order)
        {
            if (PendingOrders.Contains(order))
                PendingOrders.Remove(order);
            PendingOrders.Add(order);
        }

        public static void SubmitOrder(Order order)
        {
            // for the purposes of this demonstration, we will simply remove the order from the list
            PendingOrders.Remove(order);
        }
    }
}