﻿using System.Collections.Generic;

namespace GenMed.Data
{
    public class Order
    {
        public string Id { get; set; }
        public string AccountId { get; set; }
        public string AccountName { get; set; }
        public Address ShipTo { get; set; }
        public string DeliveryMethod { get; set; }
        public string PONumber { get; set; }
        public List<OrderItem> Items { get; set; }
    }

    public class OrderItem
    {
		public string Id {get; set;}
        public Product Product { get; set; }
        public int Quantity { get; set; }
        public string Note { get; set; }
    }
}