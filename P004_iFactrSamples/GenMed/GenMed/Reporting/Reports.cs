using System.Collections.Generic;
using iFactr.Core.Controls;
using iFactr.Core.Layers;

namespace GenMed.Reporting
{
    public class Reports : iLayer, IMasterLayer
    {
        public override void Load(Dictionary<string, string> parameters)
        {
            Title = "Reports";

            var menu = new iMenu();
            menu.Header = string.Empty;

            menu.Items.Add(new iItem("Reports/POPending", "Daily Sales") { Icon = new iFactr.Core.Controls.Icon("MenuIcons/dailysales.png") });
            menu.Items.Add(new iItem("Reports/POPending", "Plan vs. Actual") { Icon = new iFactr.Core.Controls.Icon("MenuIcons/planactual.png") });
            menu.Items.Add(new iItem("Reports/POPending", "PO Pending") { Icon = new iFactr.Core.Controls.Icon("MenuIcons/popending.png") });
            menu.Items.Add(new iItem("Reports/POPending", "Case Tracker") { Icon = new iFactr.Core.Controls.Icon("MenuIcons/casetracking.png") });

            Items.Add(menu);

            ActionButtons.Add(new Button("iFactr", "About") { Image = new Icon("as-about.png"), });
        }
    }
}