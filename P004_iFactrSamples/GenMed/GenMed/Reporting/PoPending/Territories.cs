using System;
using System.Collections.Generic;
using System.Linq;
using iFactr.Core.Layers;

namespace GenMed.Reporting.POPending
{
    class Territories : Layer
    {
        public override void Load(Dictionary<string, string> parameters)
        {
            string regionId = parameters["Region"];
            string districtId = parameters["District"];
            Data.District currentDistrict = Data.Provider.GeographyDataSet.Regions.FirstOrDefault(region => region.Id == regionId).Districts.FirstOrDefault(district => district.Id == districtId);

            Title = currentDistrict.Name;

            iList list = new iList();
            list.Header = string.Format("{0:C}", currentDistrict.TotalPOs);

            list.Items.Add(new iItem(string.Format("Reports/POPending/{0}/{1}/All", regionId, districtId), "All Territories"));

            foreach (Data.Territory territory in currentDistrict.Territories)
            {
                list.Items.Add(new RightSubtextItem(string.Format("Reports/POPending/{0}/{1}/{2}", regionId, districtId, territory.Id), territory.Name, string.Format("{0:C}", territory.TotalPOs)));
            }

            Items.Add(list);
        }
    }
}