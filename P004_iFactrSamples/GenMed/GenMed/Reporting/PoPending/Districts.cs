using System;
using System.Collections.Generic;
using System.Linq;
using iFactr.Core.Layers;

namespace GenMed.Reporting.POPending
{
    public class Districts : iLayer
    {
        public override void Load(Dictionary<string, string> parameters)
        {
            string regionId = parameters["Region"];
            Data.Region currentRegion = Data.Provider.GeographyDataSet.Regions.FirstOrDefault(region => region.Id == regionId);

            Title = currentRegion.Name;

            iList list = new iList();
            list.Header = string.Format("{0:C}", currentRegion.TotalPOs);

            list.Items.Add(new iItem(string.Format("Reports/POPending/{0}/All", currentRegion.Id), "All Districts"));

            foreach (Data.District district in currentRegion.Districts)
            {
                list.Items.Add(new RightSubtextItem(string.Format("Reports/POPending/{0}/{1}", currentRegion.Id, district.Id), district.Name, string.Format("{0:C}", district.TotalPOs)));
            }

            Items.Add(list);
        }
    }
}