﻿using System.Collections.Generic;
using System.Linq;

namespace GenMed.Reporting.POPending.Data
{
    public class Region
    {
        public string Name { get; set; }
        public string Id { get; set; }
        public List<District> Districts { get; set; }

        public IEnumerable<Account> GetAccounts()
        {
            return Districts.SelectMany(district => district.GetAccounts());
        }

        public double TotalPOs
        {
            get { return Districts.Sum(district => district.TotalPOs); }
        }

        public Region()
        {
            Districts = new List<District>();
        }
    }
}