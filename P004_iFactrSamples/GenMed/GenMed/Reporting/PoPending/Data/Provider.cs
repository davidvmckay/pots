﻿using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;

using iFactr.Core;

namespace GenMed.Reporting.POPending.Data
{
    public static class Provider
    {
        public static Geography GeographyDataSet;

        static Provider()
        {
            // load the data from the xml file as soon as it is needed
            string filePOPending = Path.Combine(iApp.Factory.ApplicationPath, "POPending.xml");

            XmlSerializer xmlPOPending = new XmlSerializer(typeof(Geography),
                new [] { 
        			typeof(Geography), 
        			typeof(List<Region>), 
        			typeof(Region), 
        			typeof(List<Territory>), 
        			typeof(Territory), 
        			typeof(List<District>), 
        			typeof(District), 
        			typeof(List<Account>), 
        			typeof(Account), 
        		});

            // iApp.File.Read will give us a byte array that can be passed to a stream for deserialization
            GeographyDataSet = (Geography)xmlPOPending.Deserialize(new MemoryStream(iApp.File.Read(filePOPending)));
        }
    }
}