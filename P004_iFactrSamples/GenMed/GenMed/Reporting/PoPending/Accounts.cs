using System;
using System.Collections.Generic;
using System.Linq;
using iFactr.Core.Layers;

namespace GenMed.Reporting.POPending
{
    public class Accounts : iLayer
    {
        public override void Load(Dictionary<string, string> parameters)
        {
            string regionId = null;
            string districtId = null;
            string territoryId = null;

            if (parameters.ContainsKey("Region"))
                regionId = parameters["Region"];

            if (parameters.ContainsKey("District"))
                districtId = parameters["District"];

            if (parameters.ContainsKey("Territory"))
                territoryId = parameters["Territory"];

            iList list = new iList();
            IEnumerable<Data.Account> accounts;

            Data.Region currentRegion = Data.Provider.GeographyDataSet.Regions.FirstOrDefault(region => region.Id == regionId);

            if (districtId != null)
            {
                Data.District currentDistrict = currentRegion.Districts.FirstOrDefault(district => district.Id == districtId);

                if (territoryId != null)
                {
                    Data.Territory currentTerritory = currentDistrict.Territories.FirstOrDefault(territory => territory.Id == territoryId);
                    accounts = currentTerritory.Accounts;
                    Title = currentTerritory.Name;
                    list.Header = string.Format("{0:C}", currentTerritory.TotalPOs);
                }
                else
                {
                    accounts = currentDistrict.GetAccounts();
                    Title = currentDistrict.Name;
                    list.Header = string.Format("{0:C}", currentDistrict.TotalPOs);
                }
            }
            else
            {
                accounts = currentRegion.GetAccounts();
                Title = currentRegion.Name;
                list.Header = string.Format("{0:C}", currentRegion.TotalPOs);
            }

            foreach (Data.Account account in accounts)
            {
                list.Items.Add(new RightSubtextItem(null, account.AccountName, string.Format("{0:C}", account.AccountPOs)));
            }

            Items.Add(list);
        }
    }
}