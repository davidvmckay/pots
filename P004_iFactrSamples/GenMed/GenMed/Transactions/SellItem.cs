﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using iFactr.Core.Layers;
using iFactr.Core.Forms;
using iFactr.Core.Controls;

namespace GenMed.Transactions.Warehouse
{
    class SellItem : Layer
    {
        public override void Load(Dictionary<string, string> parameters)
        {
            string orderId = parameters.Keys.Contains("Order") ? parameters["Order"] : null;
            string model = parameters.Keys.Contains("Model") ? parameters["Model"] : null;

            Data.Order order = Data.Provider.GetOrderById(orderId);
            Data.OrderItem orderitem = order.Items.FirstOrDefault(item => item.Product.Model == model);

            Title = order.AccountName;

            iPanel panel = new iPanel();
            panel.Header = "Item Detail";
            panel.AppendHeading(string.Format("{0} (Qty-{1})", model, orderitem.Quantity.ToString()));
            panel.AppendLine(orderitem.Product.Description);
            panel.AppendLine();
            panel.AppendItalicLine("Note: ");
            panel.AppendLine(order.PONumber == "" ? "N/A" : orderitem.Note);

            Items.Add(panel);
        }
    }
}
