using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using iFactr.Core;
using iFactr.Core.Controls;
using iFactr.Core.Layers;
using iFactr.Core.Forms;

namespace GenMed.Transactions.Warehouse
{
    class SellItemDetails : FormLayer
    {
        public override void Load(Dictionary<string, string> parameters)
        {
            string action;

            string accountId = parameters["Account"];
            string orderId = parameters["Order"];
            string model = parameters["Model"];
			string orderAction = parameters.ContainsKey("Action") ? parameters["Action"]: null;

            string typeCode = null;

            if (!string.IsNullOrEmpty(orderAction) && orderAction.Equals("Update"))
			{
				action = string.Format("Transactions/{0}/Sell/Warehouse/{1}", accountId, orderId);
			}
			else if (parameters.ContainsKey("Type"))
	        {
	            typeCode = parameters["Type"];
	            action = string.Format("Transactions/{0}/Sell/Warehouse/{1}/Items/Add/{2}/{3}", accountId, orderId, typeCode, model);
	        }
	        else
	        {
	            action = string.Format("Transactions/{0}/Sell/Warehouse/{1}", accountId, orderId, model);
	        }
            

            Data.Product product = Data.Provider.GetProductById(typeCode, model);
            Data.Order order = Data.Provider.GetOrderById(orderId);
            Data.OrderItem orderItem = null;

            if (order != null)
            {
                orderItem = order.Items.FirstOrDefault(i => i.Product == product);
            }

            Title = model;

            Fieldsets = new List<Fieldset>
            {
                new Fieldset()
                {
					Header = product.Description,
					Fields =
                    {
    				    new SelectListField("Quantity", new[] {"1", "2", "3", "4", "5", "6"})
                        {
                            SelectedIndex = orderItem != null && orderItem.Quantity > 0 ? orderItem.Quantity - 1 : 0,
    					    Label = "Quantity:"
    					},
                        new TextField("Note") { Label = "Note:", Text = orderItem != null ? orderItem.Note : string.Empty },
    				}
                }
            };

            ActionButton = string.IsNullOrEmpty(orderAction) ? new SubmitButton("Add", action) { Image = new Icon("as-add.png") } : new SubmitButton("Save", action) { Image = new Icon("as-save.png") };
            ActionParameters = parameters;
            Layout = FormLayout.Simple;
        }
    }
}
