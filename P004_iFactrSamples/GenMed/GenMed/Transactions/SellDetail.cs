﻿using System;
using System.Collections.Generic;
using System.Linq;
using iFactr.Core.Controls;
using iFactr.Core.Forms;
using iFactr.Core.Layers;

namespace GenMed.Transactions.Warehouse
{
    class SellDetail : FormLayer
    {
        public override void Load(Dictionary<string, string> parameters)
        {
            string accountId = parameters["Account"];
            string orderId = parameters.Keys.Contains("Order") ? parameters["Order"] : null;
			
            Data.Order order = Data.Provider.GetOrderById(orderId);
            Data.Account account = Data.Provider.GetAccountById(accountId);

            Title = account.Name;

            Fieldsets = new List<Fieldset>
            {
                new Fieldset()
                {
            	    Header = "Order Details",
            	    Fields =
                    {
                		new SelectListField("DeliveryMethod", new[] {"FDX - Ground", "FDX - Express", "UPS - Ground", "USPS - First Class", "USPS - Certified", "USPS - Registered"})
                        { 
                            SelectedValue = order.DeliveryMethod,
                			Label = "Delivery Method:"
                		},
                        new TextField("PONumber") { Label = "PO Number:", Text =  order != null && order.PONumber != null ? order.PONumber : string.Empty },
                	}
                }
            };

            ActionButton = new SubmitButton("Save", string.Format("Transactions/{0}/Sell/Warehouse/{1}", order.AccountId, order.Id)) { Image = new Icon() { Location = "as-save.png" } };
            Layout = FormLayout.Simple;
        }
    }
}