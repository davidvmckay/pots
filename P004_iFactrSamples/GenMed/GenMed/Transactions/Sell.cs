using System;
using System.Collections.Generic;
using System.Linq;

using iFactr.Core.Layers;
using iFactr.Core.Controls;

namespace GenMed.Transactions.Warehouse
{
    class Sell : iLayer
    {
        public override void Load(Dictionary<string, string> parameters)
        {
            string accountId = parameters["Account"];
            string orderId = parameters.Keys.Contains("Order") ? parameters["Order"] : null;
            string typeCode = parameters.Keys.Contains("Type") ? parameters["Type"] : null;
            string model = parameters.Keys.Contains("Model") ? parameters["Model"] : null;
            string delivery = parameters.Keys.Contains("DeliveryMethod") ? parameters["DeliveryMethod"] : null;
            string ponumber = parameters.Keys.Contains("PONumber") ? parameters["PONumber"] : null;
            string quantity = parameters.Keys.Contains("Quantity") ? parameters["Quantity"] : null;
            string note = parameters.Keys.Contains("Note") ? parameters["Note"] : null;
            string orderItemId = parameters.ContainsKey("OrderItemId") ? parameters["OrderItemId"] : null;

            Data.Order order = null;
            Data.Product product = null;
            Data.Account account = Data.Provider.GetAccountById(accountId);

            // if we have an orderId, retrieve that order and update it
            if (orderId != null)
            {
                order = Data.Provider.GetOrderById(orderId);
                if (order != null && (delivery != null || !string.IsNullOrEmpty(ponumber)))
                {
                    order.DeliveryMethod = delivery;
                    order.PONumber = ponumber;
                    Data.Provider.SaveOrder(order);
                }
            }
            // otherwise create a new order
            if (order == null)
            {
                order = new Data.Order()
                {
                    Id = Guid.NewGuid().ToString().Substring(0, 5),
                    AccountId = account.Id,
                    AccountName = account.Name,
                    ShipTo = account.Address,
                    DeliveryMethod = string.Empty,
                    PONumber = string.Empty,
                    Items = new List<Data.OrderItem>()
                };
                Data.Provider.SaveOrder(order);
            }

            if (typeCode != null && model != null)
            {
                product = Data.Provider.GetProductById(typeCode, model);

                // if there was no order item id passed, we know this is a new order item
                if (string.IsNullOrEmpty(orderItemId))
                {
                    order.Items.Add(
                        new Data.OrderItem()
                        {
                            Id = Guid.NewGuid().ToString().Substring(0, 5),
                            Note = note ?? string.Empty,
                            Product = product,
                            Quantity = quantity != null ? int.Parse(quantity) : 1
                        }
                    );
                }
                // else this is an existing order item and we need to update it accordingly
                else
                {
                    Data.OrderItem orderItem = order.Items.SingleOrDefault(item => item.Id == orderItemId);
                    if (orderItem != null)
                    {
                        orderItem.Note = note ?? string.Empty;
                        orderItem.Quantity = quantity != null ? int.Parse(quantity) : 1;
                    }
                }

                Data.Provider.SaveOrder(order);
            }

            Title = "Order";
            Name = "Order";

            iPanel panel = new iPanel();
            panel.Header = "Order Detail";
            panel.AppendSubHeading(account.Name);
            panel.AppendItalicLine("Ship To: ");
            panel.AppendLine(account.Address.Street1);
            panel.AppendLine(account.Address.City + ", " + account.Address.State + "  " + account.Address.Zip);
            panel.AppendLine();
            panel.AppendItalicLine("Delivery Method: ");
            panel.AppendLine(order.DeliveryMethod == "" ? "N/A" : order.DeliveryMethod);
            panel.AppendLine();
            panel.AppendItalicLine("PO Number: ");
            panel.AppendLine(order.PONumber == "" ? "N/A" : order.PONumber);


            iList list = new iList();
            list.Items.Add(new iItem(string.Format("Transactions/{0}/Sell/Warehouse/{1}/Detail", account.Id, order.Id.ToString()), "Change Details", "For this Order"));
            if (order.Items.Count == 0)
            {
                list.Items.Add(new iItem(null, "No Items", "For this Order"));
            }
            else
            {
                panel.AppendLine();
                panel.AppendLine("Total line-items: " + order.Items.Count.ToString());
                foreach (Data.OrderItem item in order.Items)
                {
                    list.Items.Add(new iItem(string.Format("Transactions/{0}/Sell/Warehouse/{1}/Items/{2}/{3}/{4}/{5}", account.Id, order.Id, typeCode, item.Product.Model, item.Id, "Update"), string.Format("{0} (Qty-{1})", item.Product.Model, item.Quantity), item.Product.Description));
                }

                if (orderId != null)
                    list.Items.Add(new iItem(string.Format("Transactions/Submit/{0}", orderId), "Process", "This Order"));
                else
                    list.Items.Add(new iItem(string.Format("Transactions/{0}/Sell/Warehouse/{1}/Submit", account.Id, order.Id), "Process", "This Order"));
            }

            Items.Add(panel);
            Items.Add(list);

            ActionButtons.Add(new Button("Add", string.Format("Transactions/{0}/Sell/Warehouse/{1}/Items/New", order.AccountId, order.Id)) { Action = Button.ActionType.Add, Image = new Icon() { Location = "as-add.png" } });
        }
    }
}
