﻿using System.Collections.Generic;
using System.Linq;
using iFactr.Core.Controls;
using iFactr.Core.Layers;

namespace GenMed.Transactions
{
    public class PendingOrders : iLayer, IMasterLayer
    {
        public override void Load(Dictionary<string, string> parameters)
        {
            string orderId = parameters.Keys.Contains("Order") ? parameters["Order"] : null;

            // if the orderId is not null, we are submitting an order
            if (orderId != null)
            {
                Data.Order order = Data.Provider.GetOrderById(orderId);
                Data.Provider.SubmitOrder(order);
            }

            Title = "Transactions";

            if (parameters.ContainsKey("Type") && parameters.ContainsKey("Model"))
            {
                Name = Name + "ProdOrder";
            }
            else
            {
                Name = "Order";
            }

            iList plist = new iList();
            plist.Header = "Pending";

            if (Data.Provider.PendingOrders.Count == 0)
                plist.Items.Add(new iItem(null, "No Pending Orders", "At this time"));
            else
            {
                foreach (Data.Order order in Data.Provider.PendingOrders)
                {
                    plist.Items.Add(new iItem(string.Format("Transactions/{0}/Sell/Warehouse/{1}", order.AccountId, order.Id), order.AccountName, string.Format("{0} {1}, {2} {3}", order.ShipTo.Street1, order.ShipTo.City, order.ShipTo.State, order.ShipTo.Zip))); 
                }
            }
            Items.Add(plist);

            iList qlist = new iList();
            qlist.Header = "Awaiting Sync";
            qlist.Items.Add(new iItem(null, "All Orders", "Have been submitted"));
            Items.Add(qlist);

            ActionButtons.Add(new Button("iFactr", "About") { Image = new Icon("as-about.png"), });
        }
    }
}