using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Views;

using iFactr.Droid;
using iFactr.Core;

namespace Droid.Container
{
    [Activity(
        MainLauncher = true,
        Theme = "@style/ApplicationTheme", 
        WindowSoftInputMode = SoftInput.AdjustPan)]
    public class MainActivity : iFactrActivity
    {
        protected override void OnCreate(Bundle bundle)
        {
            DroidFactory.Instance.MainActivity = this;
            DroidFactory.TheApp = new GenMed.App();
			iApp.OnLayerLoadComplete += (layer) => { DroidFactory.Instance.OutputLayer(layer); }; 
            base.OnCreate(bundle);
        }
    }
}