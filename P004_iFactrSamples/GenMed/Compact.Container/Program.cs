using System;
using System.Windows.Forms;
using iFactr.Core;
using iFactr.Compact;
using iFactr.Core.Layers;

namespace GenMed
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
#if (NETCF)
        [MTAThread]
#else
        [STAThread]
#endif
        static void Main()
        {
            iApp.OnLayerLoadComplete += CompactFactory.Instance.OutputLayer;
            CompactFactory.Initialize();
            CompactFactory.TheApp = new App();
            iApp.Navigate(CompactFactory.TheApp.NavigateOnLoad);
            Application.Run(CompactFactory.Instance.Form);
        }
    }
}