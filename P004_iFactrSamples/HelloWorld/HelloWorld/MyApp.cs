﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using iFactr.Core;
using iFactr.Core.Layers;
using HelloWorld.Layers;

namespace HelloWorld
{
    public class MyApp : iApp
    {
        // Add code to initialize your application here.  For more information, see http://www.factr.com/documentation
        public override void OnAppLoad()
        {
            // Set the application title
            Title = "My App";

            // Optional: Set the application style
            // Example Style.HeaderColor = new Style.Color(0, 0, 0);

            // Add navigation mappings
            NavigationMap.Add("", new Layer1());

            // Set default navigation URI
            NavigateOnLoad = "";
        }
    }
}
