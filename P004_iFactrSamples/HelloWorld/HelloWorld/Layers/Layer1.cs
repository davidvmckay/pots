﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using iFactr.Core.Layers;

namespace HelloWorld.Layers
{
    public class Layer1 : Layer
    {
        public override void Load(Dictionary<string, string> parameters)
        {
            //set layer title.  This text will appear on the header of your layer.
            Title = "MyLayer";

            //construct your layer from iFactr.Core controls.
            iBlock block = new iBlock();
            block.Append("Hello World!");
            Items.Add(block);
        }
    }
}
