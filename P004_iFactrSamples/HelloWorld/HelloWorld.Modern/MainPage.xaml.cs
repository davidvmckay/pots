﻿using iFactr.Core;
using iFactr.Core.Layers;
using iFactr.Core.Utilities.Threading;
using iFactr.Metro;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace HelloWorld.Modern
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
            MetroFactory.Initialize();
            iApp.OnLayerLoadComplete += (layer) => iApp.Thread.ExecuteOnMainThread(new ParameterDelegate((o) => MetroFactory.Instance.OutputLayer((iLayer)o)), layer);

            //Initialize your iFactr application here
            MetroFactory.TheApp = new HelloWorld.MyApp();

            iApp.Navigate(MetroFactory.TheApp.NavigateOnLoad);
        }
    }
}