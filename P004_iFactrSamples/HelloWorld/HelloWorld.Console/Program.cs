﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using iFactr.Core;
using iFactr.Core.Layers;
using iFactr.Console;

namespace HelloWorld.Console
{
    public class Program
    {
        public static void Main(string[] args)
        {
            ConsoleFactory.Initialize();
            iApp.OnLayerLoadComplete += (iLayer layer) => { ConsoleFactory.Instance.OutputLayer(layer); };

            //TODO: Instantiate your iFactr application and set the Factory App property
            ConsoleFactory.TheApp = new HelloWorld.MyApp();

            iApp.Navigate(ConsoleFactory.TheApp.NavigateOnLoad);
        }

    }
}
