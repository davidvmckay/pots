﻿using System.Windows;
using System.Windows.Data;
using iFactr.Core;
using iFactr.Core.Layers;
using iFactr.Wpf;

namespace HelloWorld.Windows
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            WpfFactory.Initialize();
            iApp.OnLayerLoadComplete += (iLayer layer) => { WpfFactory.Instance.OutputLayer(layer); };

            //TODO: Instantiate your app here
            WpfFactory.TheApp = new HelloWorld.MyApp();

            Content = WpfFactory.Instance.MainWindow;
            iApp.Navigate(WpfFactory.TheApp.NavigateOnLoad);
        }
    }
}