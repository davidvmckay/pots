﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

using iFactr.Core;
using iFactr.Data;
using iFactr.Core.Layers;
using iFactr.Utilities.NetworkResource;
using iFactr.Utilities.NetworkResource.ResourceStrategy;
using iFactr.Utilities.NetworkResource.ResourceStrategy.Cache;

using CustomerManagement.Shared.Model;

namespace CustomerManagement.Data
{
    public class CustomerProvider : Provider<Customer>
    {
        object syncLock = new object();
        public static string ServiceURI
        {
            get
            {
                //return "http://localhost/MXDemo"; // Local Server
                return "http://dev.ifactr.com/MXDemo"; // Remote Server
                //return "LocalData"; // Local Data
            }
        }

        // ctor: provide base uri, item endpoint, list endpoint, parameters
        public CustomerProvider()
            : base(ServiceURI, "customers/{customer}.xml", "customers.xml", "customer") 
        {
			// define provider initialization code here.
        }

        // bind data: creates and returns a single list item.
        public override iItem BindData(string baseUri, Customer item)
        {
            return new SubtextItem(string.Format("{0}/{1}", baseUri, item.ID), item.Name, item.Website);
        }

        // map params: maps uri parameters to object properties
        public override Dictionary<string, string> MapParams(Customer item)
        {
            Dictionary<string, string> retval = new Dictionary<string, string>();
            retval.Add("customer", item.ID);
            return retval;
        }

        // on transaction complete: logic to expire cache for refresh
        protected override void OnTransactionComplete(RestfulObject<Customer> item, string verb)
        {
            lock (syncLock)
            {
                switch (verb)
                {
                    case "POST":
                        // remove new customer placeholder from cache
                        Expire(string.Format("customers/{0}.xml", item.Object.TempID), ExpireMethodType.RemoveAll);
                        break;
                    case "PUT":
                        // expire customer cache
                        Expire(item.Object);
                        break;
                    case "DELETE":
                        break;
                }
                // expire the cache list and refresh
                ExpireCacheList();
                RefreshCustomerList();
            }
        }
        protected override void OnTransactionFailed(RestfulObject<Customer> item, string verb, Exception ex)
        {
            base.OnTransactionFailed(item, verb, ex);
        }
        protected override void OnTransactionError(RestfulObject<Customer> item, string verb, System.Net.HttpStatusCode error)
        {
            base.OnTransactionError(item, verb, error);
        }

        #region Dashboard Helpers

        public iBlock GetDashboardBlock()
        {
            iBlock block = new iBlock();
            block.InsertImageFloatLeft("provider.png");

            // get cache metrics
            var memcount = Providers.Customers.CacheList.Count.ToString();
            var indexcount = IndexedCustomers.Count.ToString();
            var filecount = StoredCustomers.Count.ToString();

            block.AppendLine();
            block.AppendHeading("Customer Provider");
            block.AppendLine(this.BaseUri);
            block.AppendLine();
            block.AppendLabeledTextLine("Data Format", this.Format.ToString());
            block.AppendLine();
            block.AppendLabeledTextLine("Provider Methods", this.ProviderMethods.ToString());
            block.AppendLine();
            block.AppendLabeledTextLine("Resource Strategy", this.ResourceStrategy.ToString());

            block.AppendLine();
            block.AppendLine();
            block.AppendLine();
            
            //block.AppendSubHeading("Endpoints");
            //block.AppendLine();
            //block.AppendItalic("List: "); block.AppendLine(this.ListRelativeUri);
            //block.AppendItalic("Resource: "); block.AppendLine(this.ObjectRelativeUri.Replace("{", "[").Replace("}", "]"));
            //block.AppendLine();

            block.AppendSubHeading("Cache Statistics");
            block.AppendHorizontalRule();
            block.AppendLine();
            block.AppendLine();
            block.AppendBoldItalic("Cache Method:   "); block.AppendLine(this.CacheMethod.ToString());
            block.AppendBoldItalic("Stale Method:   "); block.AppendLine(this.CacheStaleMethod.ToString());
            block.AppendBoldItalic("Default Expiration:   "); block.AppendLine(FormattedExpiration);
            block.AppendLine();
            block.AppendBoldItalic("Items in Memory:   "); block.AppendLine(memcount);
            block.AppendBoldItalic("Items in Index:   "); block.AppendLine(indexcount);
            block.AppendBoldItalic("Items in Storage:   "); block.AppendLine(filecount);
            block.AppendLine();
            block.AppendLine();

            block.AppendSubHeading("Queue Statistics");
            block.AppendHorizontalRule();
            block.AppendLine();
            block.AppendLine();
            block.AppendBoldItalicLine(FormattedEnabled);
            block.AppendLine();
            block.AppendBoldItalic("Response Timeout:   "); block.AppendLine(FormattedTimeout);
            block.AppendLine();
            block.AppendBoldItalic("Pending Transactions:   "); block.AppendLine(Providers.Customers.QueueList.Count.ToString());
            block.AppendLine();

            return block;
        }

        string FormattedExpiration
        {
            get
            {
                string retval = null;
                var strb = new StringBuilder();

                strb.Append(DefaultExpiration.Days > 0 ? string.Format("{0} day{1} ", DefaultExpiration.Days, DefaultExpiration.Days > 1 ? "s" : string.Empty) : string.Empty);
                strb.Append(DefaultExpiration.Hours > 0 ? string.Format("{0} hour{1} ", DefaultExpiration.Hours, DefaultExpiration.Hours > 1 ? "s" : string.Empty) : string.Empty);
                strb.Append(DefaultExpiration.Minutes > 0 ? string.Format("{0} minute{1} ", DefaultExpiration.Minutes, DefaultExpiration.Minutes > 1 ? "s" : string.Empty) : string.Empty);
                strb.Append(DefaultExpiration.Seconds > 0 ? string.Format("{0} second{1} ", DefaultExpiration.Seconds, DefaultExpiration.Seconds > 1 ? "s" : string.Empty) : string.Empty);
                strb.Append(DefaultExpiration.Milliseconds > 0 ? string.Format("{0} millisecond{1}", DefaultExpiration.Milliseconds.ToString(), DefaultExpiration.Milliseconds > 1 ? "s" : string.Empty) : string.Empty);
                retval = strb.ToString();
                
                return string.IsNullOrEmpty(retval) ? "Immediate" : retval;
            }
        }
        string FormattedTimeout
        {
            get { return string.Format("{0} seconds", (this.ResponseTimeout / 1000).ToString()); }
        }
        string FormattedEnabled { get { return this.QueueEnabled ? "Queue is Enabled" : "Queue is Disabled"; } }

        #endregion

        #region Cache Helpers

        public static CacheIndex CustomerIndex
        {
            get 
            { 
                return CacheIndexMap.GetFromUri(ServiceURI); 
            }
        }

        static List<Customer> localCustomers = null;
        public static List<Customer> CustomerList
        {
            get
            {
                if (localCustomers == null)
                {
                    localCustomers = Providers.Customers.GetList().Object;
                }
                var retval = Providers.Customers.IsCachingList ? Providers.Customers.CacheList : localCustomers;
                retval.Sort((x, y) => { return x.Name.CompareTo(y.Name); });
                return retval;
            }
        }

        public static List<CacheIndexItem> IndexedCustomers
        {
            get
            {
                var retval = new List<CacheIndexItem>();
                foreach (CacheIndexItem item in CustomerIndex)
                {
                    // exclude list from customers indexed
                    if (item.RelativeUri != "customers.xml" && item.RelativeUri != "customers.json")
                    {
                        retval.Add(item);
                    }
                }
                return retval; 
            }
        }

        public static List<CacheIndexItem> StoredCustomers
        {
            get
            {
                var retval = new List<CacheIndexItem>();
                foreach (CacheIndexItem item in IndexedCustomers)
                {
                    var filePath = CustomerIndex.GetCachePath(item);
                    if (iApp.File.Exists(filePath))
                    {
                        retval.Add(item);
                    }
                }
                return retval;
            }
        }

        public static void RefreshCustomerList()
        {
            localCustomers = Providers.Customers.GetList().Object;
        }

        static CacheIndexItem GetCustomerCache(string customer)
        {
            return CustomerIndex.Get(string.Format("{0}customers/{1}.xml", CustomerIndex.BaseUri, customer), false);
        }

        public static bool IsCached(string customer)
        {
            CacheIndexItem item = GetCustomerCache(customer);
            if (item != null)
            {
                switch (Providers.Customers.CacheMethod)
                {
                    case CacheMethodType.Direct:
                    case CacheMethodType.PersistOnly:
                        return true;
                    default:
                        return  Providers.Customers.CacheInStorage && item.IsDownloaded;
                }
            }
            else
                return false;
        }

        public static string GetCachePath(CacheIndexItem item)
        {
            return CustomerIndex.GetCachePath(item).Replace(CustomerIndex.CachePath, string.Empty);
        }

        public bool CacheInStorage
        {
            get
            {
                return this.ResourceStrategy == ResourceStrategyType.Cache;
            }
        }

        public bool CacheInMemory
        {
            get { return this.ResourceStrategy != ResourceStrategyType.LocalFile &&
                         this.CacheMethod != CacheMethodType.None; }
        }

        public bool IsCaching { get { return CacheInStorage || CacheInMemory; } }
        public bool IsCachingList { get { return this.CacheMethod != CacheMethodType.None && this.CacheMethod != CacheMethodType.PersistOnly; } }

        #endregion

        #region Queue Helpers

        public static string GetTempID()
        {
            return localCustomers.Min(c => c.ID);
        }
        public bool IsQueued(Customer cust)
        {
            return QueueList.Contains(cust);
        }
        public bool CanAdd { get { return ProviderMethods.HasFlag(ProviderMethod.POST); } }
        public bool CanChange { get { return ProviderMethods.HasFlag(ProviderMethod.PUT); } }
        public bool CanDelete { get { return ProviderMethods.HasFlag(ProviderMethod.DELETE); } }

        #endregion
    }
#if PocketPC
    public static class MyHelper
    {
        public static bool HasFlag(this Enum thisEnum, Enum enumValue)
        {
            var intValue = Convert.ToInt32(enumValue);
            var baseValue = Convert.ToInt32(thisEnum);

            return (intValue & baseValue) == 0;
        }
    }
#endif
}
