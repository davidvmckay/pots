﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using iFactr.Data;

using CustomerManagement.Shared.Model;

namespace CustomerManagement.Data
{
  public static class Providers
  {
    // define registry
    static ProviderRegistry _providers = new ProviderRegistry();

    // static ctor: register providers
    static Providers()
    {
      _providers.Register(new CustomerProvider());
    }

    // static accessor:  for simplified usage
    public static CustomerProvider Customers 
    {
      get { return _providers.GetProvider(typeof(Customer)) as CustomerProvider; }
    }
  }
}
