﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using iFactr.Core;
using iFactr.Core.Layers;
using iFactr.Utilities.NetworkResource.ResourceStrategy.Cache;

using CustomerManagement.Shared.Model;
using CustomerManagement.Data;
using CustomerManagement.Layers;

namespace CustomerManagement
{
    public class App : iApp
    {
        // Add code to initialize application here.  For more information, see http://www.factr.com/documentation
        public override void OnAppLoad()
        {
            // set the application title
            Title = "Customer Management";

            // set cache serialization interval to immediate
            CacheIndex.SerializationInterval = 0;

            // Add navigation mappings
            NavigationMap.Add("", new MainTabs());

            NavigationMap.Add("Dashboard", new Dashboard());
            
            NavigationMap.Add("Customers", new CustomerList());
            NavigationMap.Add("Customers/{Customer}", new CustomerList());
            NavigationMap.Add("Customers/{Customer}/Detail", new CustomerDetails());
            NavigationMap.Add("Customers/{Customer}/{Action}", new CustomerEdit());

            NavigationMap.Add("Indexed", new IndexedCustomers());
            NavigationMap.Add("Indexed/{Customer}", new IndexedCustomers());
            NavigationMap.Add("Indexed/{Customer}/{Action}", new IndexedDetails());

            NavigationMap.Add("Stored", new StoredCustomers());
            NavigationMap.Add("Stored/{Customer}", new StoredDetails());
           
            NavigationMap.Add("Pending", new PendingCustomers());
            NavigationMap.Add("Pending/{Customer}", new PendingDetails());

            // set default navigation uri
            NavigateOnLoad = "";
        }
    }
}
