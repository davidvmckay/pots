﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using iFactr.Core.Forms;
using iFactr.Core.Layers;
using iFactr.Core.Controls;
using iFactr.Data;
using iFactr.Utilities.NetworkResource.ResourceStrategy.Cache;

using CustomerManagement.Data;
using CustomerManagement.Shared.Model;

namespace CustomerManagement.Layers
{
    class IndexedDetails : Layer, IDetailLayer
    {
        public override void Load(Dictionary<string, string> parameters)
        {
            Title = "Indexed Details";

            // get layer parameters
            string customer = parameters.ContainsKey("Customer") ? parameters["Customer"] : string.Empty;
            string action = parameters.ContainsKey("Action") ? parameters["Action"] : string.Empty;
            bool refresh = action.EndsWith("Refresh");

            // get cache index item, and customer
            CacheIndexItem item = CustomerProvider.CustomerIndex.Get(string.Format("{0}customers/{1}.xml", CustomerProvider.CustomerIndex.BaseUri, customer), false);
            
            // if index item is not present, navigate to dashboard
            if (item == null)
            {
                this.CancelLoadAndNavigate("Dashboard");
                return;
            }

            Customer cust = Providers.Customers.GetByUri(item.RelativeUri).Object;
            
            switch (action)
            {
                case "Load":
                case "LoadRefresh":
                    LoadDetails(cust, item);
                    LoadButtons(cust, refresh);
                   break;                   
                case "Expire":
                case "ExpireRefresh":
                    Providers.Customers.Expire(cust, ExpireMethodType.ExpireRemove); // default
                    this.CancelLoadAndNavigate(string.Format("{0}/{1}", refresh ? "Customers" : "Indexed", cust.ID));
                    break;
                case "ExpireOnly":
                case "ExpireOnlyRefresh":
                    Providers.Customers.Expire(cust, ExpireMethodType.ExpireOnly);
                    this.CancelLoadAndNavigate(string.Format("{0}/{1}", refresh ? "Customers" : "Indexed", cust.ID));
                    break;
                case "ExpireRemoveAll":
                case "ExpireRemoveAllRefresh":
                    Providers.Customers.Expire(cust, ExpireMethodType.RemoveAll);
                    this.CancelLoadAndNavigate(string.Format("{0}/{1}", refresh ? "Customers" : "Indexed", cust.ID));
                    break;
                case "ExpireRemoveCache":
                case "ExpireRemoveCacheRefresh":
                    Providers.Customers.Expire(cust, ExpireMethodType.RemoveCache);
                    this.CancelLoadAndNavigate(string.Format("{0}/{1}", refresh ? "Customers" : "Indexed", cust.ID));
                    break;
                case "ExpireStaleCache":
                case "ExpireStaleCacheRefresh":
                    Providers.Customers.Expire(cust, ExpireMethodType.StaleCache);
                    this.CancelLoadAndNavigate(string.Format("{0}/{1}", refresh ? "Customers" : "Indexed", cust.ID));
                    break;
                default:
                    break;
            }
        }

        void LoadDetails(Customer cust, CacheIndexItem item)
        {
            if (cust != null)
            {
                // build detail block
                iBlock block = new iBlock();

				block.InsertImageFloatLeft(item.IsExpired ? DetailCustExpireIconPath : DetailCustCacheIconPath);
                block.AppendBoldLine(cust.Name);
                block.AppendLine(item.RelativeUri);
                block.AppendLine();
                block.AppendBoldLine("Downloaded");
                block.AppendLine(!item.IsDownloaded ? "Not Downloaded" : string.Format(item.Downloaded.ToLocalTime().ToString()));
                block.AppendLine();
                block.AppendBoldLine("Stale");
                block.AppendLine(!item.IsDownloaded ? "--" : string.Format(item.AttemptToRefresh.ToLocalTime().ToString()));
                block.AppendLine();
                block.AppendBoldLine("Expires");
                block.AppendLine(!item.IsDownloaded ? "--" : string.Format(item.Expiration.ToLocalTime().ToString()));

                Items.Add(block);
            }
        }

        void LoadButtons(Customer cust, bool refresh)
        {
            Fieldset actions = new Fieldset();
            actions.Header = "Cache Expire Operations";
            actions.Fields.Add(new ButtonField("Expire", string.Format("Indexed/{0}/{1}", cust.ID, refresh ? "ExpireRefresh" : "Expire")) { Label = "Expire Remove", Action = Button.ActionType.Edit });
            actions.Fields.Add(new ButtonField("ExpireOnly", string.Format("Indexed/{0}/{1}", cust.ID, refresh ? "ExpireOnlyRefresh" : "ExpireOnly")) { Label = "Expire Only", Action = Button.ActionType.Edit });
            actions.Fields.Add(new ButtonField("ExpireRemoveAll", string.Format("Indexed/{0}/{1}", cust.ID, refresh ? "ExpireRemoveAllRefresh" : "ExpireRemoveAll")) { Label = "Remove All", Action = Button.ActionType.Edit });
            actions.Fields.Add(new ButtonField("ExpireRemoveCache", string.Format("Indexed/{0}/{1}", cust.ID, refresh ? "ExpireRemoveCacheRefresh" : "ExpireRemoveCache")) { Label = "Remove Cache", Action = Button.ActionType.Edit });
            actions.Fields.Add(new ButtonField("ExpireStaleCache", string.Format("Indexed/{0}/{1}", cust.ID, refresh ? "ExpireStaleCacheRefresh" : "ExpireStaleCache")) { Label = "Stale Cache", Action = Button.ActionType.Edit });
            Items.Add(actions);
        }

		internal const string DetailCustExpireIconPath = "DetailPane_Cust-Exp.png";
		internal const string DetailCustCacheIconPath = "DetailPane_Cust-Cache.png";
    }
}
