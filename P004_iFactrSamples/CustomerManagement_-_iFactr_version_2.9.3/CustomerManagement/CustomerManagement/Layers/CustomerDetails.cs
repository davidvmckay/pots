﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using iFactr.Core.Layers;
using iFactr.Core.Forms;
using iFactr.Core.Controls;

using CustomerManagement.Data;
using CustomerManagement.Shared.Model;

namespace CustomerManagement.Layers
{
    class CustomerDetails : Layer, IDetailLayer
    {
        public override void Load(Dictionary<string, string> parameters)
        {
            Title = "Customer Details";

            // get layer parameters
            string customer = parameters.ContainsKey("Customer") ? parameters["Customer"] : string.Empty;

            // build detail block
            if (!string.IsNullOrEmpty(customer))
            {
                iBlock block = new iBlock();
                // get customer by id
                Customer cust = Providers.Customers.Get(customer).Object;
                // get customer by uri
                //Customer cust = Providers.Customers.GetByUri(string.Format("customers/{0}.xml", customer)).Object;
                if (cust != null)
                {
					block.InsertImageFloatLeft(DetailImagePath);
                    block.AppendBoldLine(cust.Name);
                    block.AppendLine(cust.Website);
                    block.AppendLine(cust.PrimaryPhone);
                    block.AppendLine();
                    block.AppendLine(string.Format("Pending Transactions: {0}", Providers.Customers.QueueList.Count(c => c.ID == cust.ID)));
                }
                else
                {
                    block.AppendLine("No Customer Data Available");
                }
                Items.Add(block);

                // add edit and delete buttons
                Fieldset actions = new Fieldset();
                if (Providers.Customers.CanChange)
                    actions.Fields.Add(new ButtonField("Edit", string.Format("Customers/{0}/Load", cust.ID)) { Label = "Edit Customer", Action = Button.ActionType.Edit });
                if (Providers.Customers.CanDelete)
                    actions.Fields.Add(new ButtonField("Delete", string.Format("Customers/{0}/Delete", cust.ID)) { Label = "Delete Customer", Action = Button.ActionType.Delete, ConfirmationText = "Are you sure you want to delete this customer?" });
                if (actions.Fields.Count > 0)
                    Items.Add(actions);

                // add edit and delete buttons
                //if ( Providers.Customers.CanChange)
                //    ActionButtons.Add(new Button("Edit Customer", string.Format("Customers/{0}/Load", cust.ID)) { Action = Button.ActionType.Edit });
                //if (Providers.Customers.CanDelete)
                //    ActionButtons.Add(new Button("Delete Customer", string.Format("Customers/{0}/Delete", cust.ID)) { Action = Button.ActionType.Delete });
            }
        }
    	
		internal const string DetailImagePath = "DetailPane_Cust.png";
	}
}
