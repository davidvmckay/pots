﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using iFactr.Core.Layers;

using CustomerManagement.Data;

namespace CustomerManagement.Layers
{
    class Dashboard : Layer, IDetailLayer
    {
        public override void Load(Dictionary<string, string> parameters)
        {
            Title = "Provider Dashboard";

            Items.Add(Providers.Customers.GetDashboardBlock());
        }
    }
}
