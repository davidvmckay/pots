﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using iFactr.Core.Layers;
using iFactr.Core.Controls;
using iFactr.Utilities.NetworkResource.ResourceStrategy.Cache;

using CustomerManagement.Data;
using CustomerManagement.Shared.Model;

namespace CustomerManagement.Layers
{
    class StoredCustomers : Layer, IMasterLayer
    {
        public override void Load(Dictionary<string, string> parameters)
        {
            Title = "Stored Customers";

            string customer = parameters.ContainsKey("Customer") ? parameters["Customer"] : string.Empty;

            if (customer != string.Empty)
                this.DetailLink = new Link(string.Format(string.Format("Stored/{0}/Load", customer)));
            else
                this.DetailLink = new Link("Dashboard");
            
            // build cached customer list
            iList list = new iList();
            foreach (CacheIndexItem item in CustomerProvider.StoredCustomers)
            {
                Customer cust = Providers.Customers.GetByUri(item.RelativeUri).Object;
                //Customer cust = Providers.Customers.Get(App.GetCustomerIdFromIndex(item)).Object;
                list.Items.Add(new SubtextItem(string.Format("Stored/{0}", cust.ID), cust.Name, item.RelativeUri) 
                    { Icon = new Icon("file.png") });
            }
            if (list.Items.Count == 0)
                list.Items.Add(new SubtextItem(null, "No Customers", "have been stored"));

            list.Items.Sort((x, y) => { return x.Text.CompareTo(y.Text); });
            Items.Add(list);

            ActionButtons.Add(new Button(string.Empty, "Dashboard") { Text = "Dashboard" });
        }
    }
}
