﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using iFactr.Core.Layers;
using iFactr.Core.Controls;
using iFactr.Utilities.NetworkResource.ResourceStrategy.Cache;

using CustomerManagement.Data;
using CustomerManagement.Shared.Model;

namespace CustomerManagement.Layers
{
    class IndexedCustomers : Layer, IMasterLayer
    {
        public override void Load(Dictionary<string, string> parameters)
        {
            Title = "Indexed Customers";

            string customer = parameters.ContainsKey("Customer") ? parameters["Customer"] : string.Empty;

            if (customer != string.Empty)
                this.DetailLink = new Link(string.Format(string.Format("Indexed/{0}/Load", customer)));
            else
                this.DetailLink = new Link("Dashboard");
            
            // build cached customer list
            iList list = new iList();
            foreach (CacheIndexItem item in CustomerProvider.IndexedCustomers)
            {
                Customer cust = Providers.Customers.GetByUri(item.RelativeUri).Object;
                //Customer cust = Providers.Customers.Get(App.GetCustomerIdFromIndex(item)).Object;
                list.Items.Add(new SubtextItem(string.Format("Indexed/{0}/Load", cust.ID), cust.Name, item.RelativeUri) 
		               { Icon = new Icon(item.IsExpired ? IndexedCustomers.CustExpireIconPath : IndexedCustomers.CustCacheIconPath) });
            }
            if (list.Items.Count == 0)
                list.Items.Add(new SubtextItem(null, "No Customers", "have been cached"));

            list.Items.Sort((x, y) => { return x.Text.CompareTo(y.Text); });
            Items.Add(list);

            ActionButtons.Add(new Button(string.Empty, "Dashboard") { Text = "Dashboard" });
        }

		internal const string CustExpireIconPath = "cust-exp.png";
		internal const string CustCacheIconPath = "cust-cache.png";
    }
}
