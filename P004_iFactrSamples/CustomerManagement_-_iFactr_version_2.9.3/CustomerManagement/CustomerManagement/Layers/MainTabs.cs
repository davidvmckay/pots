﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using iFactr.Core.Layers;
using iFactr.Core.Controls;

namespace CustomerManagement.Layers
{
    class MainTabs : NavigationTabs
    {
        public override void Load(Dictionary<string, string> parameters)
        {
            TabItems.Add(new Tab("Customer List", "Customers", "tb-List.png", true));
			TabItems.Add(new Tab("Customer Index", "Indexed", "tb-Index.png", true));
			TabItems.Add(new Tab("Customer Store", "Stored", "tb-Files.png", true));
			TabItems.Add(new Tab("Customer Queue", "Pending", "tb-Queue.png", true));
        }
    }
}
