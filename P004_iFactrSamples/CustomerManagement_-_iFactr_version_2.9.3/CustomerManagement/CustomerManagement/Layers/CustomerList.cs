﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using iFactr.Core.Layers;
using iFactr.Core.Controls;

using iFactr.Data;

using CustomerManagement.Data;
using CustomerManagement.Shared.Model;

namespace CustomerManagement.Layers
{
    public class CustomerList : Layer, IMasterLayer
    {
        public override void Load(Dictionary<string, string> parameters)
        {
            Title = "Customer List";

            string customer = parameters.ContainsKey("Customer") ? parameters["Customer"] : string.Empty;

            if (customer == "Refresh")
            {
                CustomerProvider.RefreshCustomerList();
                this.CancelLoadAndNavigate("Customers");
            }
            // build customer list
            SearchList custlist = new SearchList();
            foreach (Customer cust in CustomerProvider.CustomerList)
            {
            	string link = null;
            	if (App.Factory.LargeFormFactor)
	            	link = string.Format("Customers/{0}", cust.ID);
            	else
	            	link = string.Format("Customers/{0}/Detail", cust.ID);
            
                var listitem = new SubtextItem(link, cust.Name, cust.Website) { Icon = new Icon(CustomerList.CustImagePath) };

                listitem.Button = CustomerProvider.IsCached(cust.ID) || cust.ID == customer && !Providers.Customers.IsQueued(cust) ?
                        new Button(string.Empty, string.Format("Indexed/{0}/LoadRefresh", cust.ID)) :
                        null;

                custlist.Items.Add(listitem);
            }
            Items.Add(custlist);

            // add detail link if customer provided
            if (customer != string.Empty)
                this.DetailLink = new Link(string.Format(string.Format("Customers/{0}/Detail", customer)));
            else
                this.DetailLink = new Link("Dashboard");

            // add buttons
            if (Providers.Customers.CanAdd)
                ActionButtons.Add(new Button(string.Empty, "Customers/0/Load") { Action = Button.ActionType.Add, Text = "Add" });
            ActionButtons.Add(new Button(string.Empty, "Customers/Refresh") { Text = "Refresh List" });
            ActionButtons.Add(new Button(string.Empty, "Dashboard") { Text = "Dashboard" });
        }

		internal const string CustImagePath = "cust.png";
    }
}
