﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using iFactr.Core;
using iFactr.Core.Controls;
using iFactr.Core.Layers;
using iFactr.Core.Forms;

using CustomerManagement.Data;
using CustomerManagement.Shared.Model;

namespace CustomerManagement.Layers
{
    class CustomerEdit : FormLayer, IDetailLayer
    {
        /// <summary>
        /// Loads the specified parameters.
        /// </summary>
        /// <param name="parameters">The parameters.</param>
        public override void Load(Dictionary<string, string> parameters)
        {
            Title = "Customer Details";

            // get layer parameters
            string customer = parameters.ContainsKey("Customer") ? parameters["Customer"] : "0";
            string action = parameters.ContainsKey("Action") ? parameters["Action"] : string.Empty;

            // set action button
            ActionButton = new SubmitButton("Save", string.Format("Customers/{0}/{1}", customer, customer == "0" ? "Create" : "Update"));

            // load customer if valid id provided
            Customer cust = customer != "0" ? Providers.Customers.Get(customer).Object : null;

            // build form and/or process transaction
            Fieldset form = new Fieldset();
            switch (action)
            {
                case "Load": // build form and populate fields
                    if (cust != null)
                    {
                        form.Header = "Change Customer";
                        form.Fields.Add(new TextField("Name") { Label = "Name", Text = cust.Name });
                        form.Fields.Add(new TextField("Website") { Label = "Website", Text = cust.Website });
                        form.Fields.Add(new TextField("PrimaryPhone") { Label = "Primary Phone", Text = cust.PrimaryPhone });
                    }
                    else
                    {
                        form.Header = "New Customer";
                        form.Fields.Add(new TextField("Name") { Placeholder = "Name" });
                        form.Fields.Add(new TextField("Website") { Placeholder = "Website" });
                        form.Fields.Add(new TextField("PrimaryPhone") { Placeholder = "Primary Phone" });
                    }
                    break;
                case "Create": // create a new customer
                    cust = new Customer();
                    cust.Name = parameters.ContainsKey("Name") ? parameters["Name"] : string.Empty;
                    cust.Website = parameters.ContainsKey("Website") ? parameters["Website"] : string.Empty;
                    cust.PrimaryPhone = parameters.ContainsKey("PrimaryPhone") ? parameters["PrimaryPhone"] : string.Empty;

                    Providers.Customers.Add(cust);
                    this.CancelLoadAndNavigate("Customers");
                    break;
                case "Update": // update an existing customer
                    if (cust != null)
                    {
                        cust.Name = parameters.ContainsKey("Name") ? parameters["Name"] : cust.Name;
                        cust.Website = parameters.ContainsKey("Website") ? parameters["Website"] : cust.Website;
                        cust.PrimaryPhone = parameters.ContainsKey("PrimaryPhone") ? parameters["PrimaryPhone"] : cust.PrimaryPhone;

                        Providers.Customers.Change(cust);
                        this.CancelLoadAndNavigate("Customers");
                    }
                    else
                    {
                        form.Fields.Add(new LabelField("Error") { Label = "Error loading customer for update." });
                    }
                    break;
                case "Delete": // delete an existing customer
                    if (cust != null)
                    {
                        Providers.Customers.Delete(cust);
                        this.CancelLoadAndNavigate("Customers");
                    }
                    else
                    {
                        form.Fields.Add(new LabelField("Error") { Label = "Error loading customer for delete." });
                    }
                    break;
                default:
                    form.Header = "New Customer";
                    form.Fields.Add(new TextField("Name") { Placeholder = "Name" });
                    form.Fields.Add(new TextField("Website") { Placeholder = "Website" });
                    form.Fields.Add(new TextField("PrimaryPhone") { Placeholder = "Primary Phone" });
                    break;
            }
            Fieldsets.Add(form);
        }
    }
}
