﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using iFactr.Core.Forms;
using iFactr.Core.Layers;
using iFactr.Core.Controls;
using iFactr.Data;
using iFactr.Utilities.NetworkResource.ResourceStrategy.Cache;

using CustomerManagement.Data;
using CustomerManagement.Shared.Model;

namespace CustomerManagement.Layers
{
    class StoredDetails : Layer, IDetailLayer
    {
        public override void Load(Dictionary<string, string> parameters)
        {
            Title = "Stored Details";

            // get layer parameters
            string customer = parameters.ContainsKey("Customer") ? parameters["Customer"] : string.Empty;

            // get cache index item, and customer
            CacheIndexItem item = CustomerProvider.CustomerIndex.Get(string.Format("{0}customers/{1}.xml", CustomerProvider.CustomerIndex.BaseUri, customer), false);
            Customer cust = Providers.Customers.GetByUri(item.RelativeUri).Object;

            if (cust != null)
            {
                // build detail block
                iBlock block = new iBlock();

                block.InsertImageFloatLeft("file.png");
                block.AppendBoldLine(cust.Name);
                block.AppendLine(item.RelativeUri);
                block.AppendLine();
                block.AppendBoldLine("Downloaded");
                block.AppendLine(!item.IsDownloaded ? "Not Downloaded" : string.Format(item.Downloaded.ToLocalTime().ToString()));
                block.AppendLine();
                block.AppendBoldLine("Indexed Location");                
                block.AppendLine(CustomerProvider.GetCachePath(item));
                
                Items.Add(block);
            }            
        }
    }
}
