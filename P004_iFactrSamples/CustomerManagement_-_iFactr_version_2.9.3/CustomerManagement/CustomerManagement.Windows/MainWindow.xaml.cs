﻿using System.Windows;
using System.Windows.Data;
using iFactr.Core;
using iFactr.Core.Layers;
using iFactr.Wpf;

namespace CustomerManagement.Windows
{
  /// <summary>
  /// Interaction logic for MainWindow.xaml
  /// </summary>
  public partial class MainWindow : Window
  {
    public MainWindow()
    {
      InitializeComponent();
      iApp.OnLayerLoadComplete += (iLayer layer) => { WpfFactory.Instance.OutputLayer(layer); };
      WpfFactory.Initialize();

      //TODO: Instantiate your app here
      WpfFactory.TheApp = new CustomerManagement.App();

      iApp.Navigate(WpfFactory.TheApp.NavigateOnLoad);
      Content = WpfFactory.Instance.MainWindow;
    }
  }
}