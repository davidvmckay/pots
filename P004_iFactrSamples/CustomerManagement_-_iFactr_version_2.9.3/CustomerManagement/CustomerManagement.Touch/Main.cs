using System;
using System.Collections.Generic;
using System.Linq;

using iFactr.Core;
using iFactr.Core.Layers;
using iFactr.Touch;

using MonoTouch.Foundation;
using MonoTouch.UIKit;

namespace CustomerManagement.Touch
{
    public class Application
    {
        static void Main (string[] args)
        {
            UIApplication.Main (args, null, "AppDelegate");
        }
    }
    
    [Register ("AppDelegate")]
    public partial class AppDelegate : UIApplicationDelegate
    {
        public override bool FinishedLaunching (UIApplication app, NSDictionary options)
        {
            TouchFactory.Initialize();
            iApp.OnLayerLoadComplete += (iLayer layer) => { InvokeOnMainThread(delegate { TouchFactory.Instance.OutputLayer(layer); } );  };
            TouchFactory.TheApp = new CustomerManagement.App();
            iApp.Navigate(TouchFactory.TheApp.NavigateOnLoad);
            TouchFactory.Instance.InitializeViews();
            return true;
        }
        
        public override void OnActivated (UIApplication application)
        {
        }
    }
}
